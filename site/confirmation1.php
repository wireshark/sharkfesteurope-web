<?php include "header.php"; ?>

<div class="container">
	<div class="row">
		<div class="col-lg-12 about-text">
			<h2>REGISTRATION PAYMENT CONFIRMATION</h2>
			<p>2-Day Troubleshooting with Wireshark Course Early Bird<br>
               November 6th – 7th, 2017<br>
               Palacio Estoril, Portugal 
            </p>
			<h2>Thank You!</h2>
                  <p>You’ve successfully completed the payment portion of Laura Chappell’s 2-day “Troubleshooting with Wireshark” course registration process.<p>
            <h2>PayPal Confirmation Email</h2>
                  <p>You will receive an email confirmation from Paypal indicating that your payment has been processed.</p>
            <h2>Registration Confirmation</h2>
                  <p>A separate email with 2-day “Troubleshooting with Wireshark” course details will be sent within one week of payment confirmation.</p>
			<h2>Registration Details Completion Survey</h2>
                <p>A survey requesting additional registration details from each attendee will be sent before the conference start date. Conference staff require this information (dietary and physical access restrictions, emergency contacts, e.g.) to create badges and ensure a smooth check-in process.</p>
            <h2>Overview of SharkFest’17 Europe Schedule</h2>
                <p><strong>Monday, November 6th</strong></p>
                <ul>
                    <li>• Laura Chappell’s “Troubleshooting with Wireshark” Course, 9am – 5pm</li>
                </ul>
                <p><strong>Tuesday, November 7th</strong></p>
                <ul>
                    <li>Laura Chappell’s “Troubleshooting with Wireshark” Course, 9am – 5pm</li>
                </ul>
            <h2>Hotel Reservations</h2>
                <p>The Palacio Estoril is the exclusive lodging option selected for Laura Chappell’s 2-day “Troubleshooting with Wireshark” course that precedes the start of SharkFest. The conference has negotiated exceptional rates for this 5-star hotel with the intention of creating:</p>
                <ul>
                    <li>• An immersive Wireshark learning experience in a superior environment that inspires networking and socializing.</li>
                    <li>• A setting with amenities for attendees to relax and refresh while increasing their Wireshark skill set.</li>
                </ul>
            <h3>To encourage attendees to choose this lodging option, every Troubleshooting registrant who reserves a room at the Hotel Palacio by August 1st will receive an AirPcap Nx adapter and other welcome gifts at check-in.</h3>
            <h2>Questions?</h2>
                <p>Please contact <a href="mailto:info@wiresharktraining.com">info@wiresharktraining.com</p>
                <p>We look forward to seeing you in class in November!</p>

		</div>
	</div>
</div>


<?php include "footer.php"; ?>
