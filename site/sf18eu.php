<?php include "header.php"; ?>
   <!-- Wrap all content -->
   <div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf18eu-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'18 EUROPE Retrospective</h1>
            <p>October 29th - November 2nd, 2018<br>Imperial Riding School Renaissance Hotel | Vienna, Austria</p>
        </div>
    </div>
    <section id="main-content">
            <div class="wrapper retrospective-page"">

               

                        
                <!-- /HEADER -->

                <!-- Content area -->
                
                <div class="content-area">

                    <!-- PAGE BLOG -->
                    <section class="services">
                    <div class="container" id="content2">
                    <div class="row">

                    <!-- Content -->
                    <section id="content5" class="content col-sm-7 col-md-8 col-lg-8">

                        <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                           <div class="post-media retro-image">
                                
                            </div>
                            <div class="post-header">
                            </div>
                            <div class="post-body">
                                <div class="post-excerpt">
                                    <p></p>     
                                    <h3 class="post-title2">Keynote Presentation</h3>
                                    <p>Coming Soon!</p>
                                    <!--<div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/HiofIifbitQ" frameborder="0" allowfullscreen></iframe>
                                    </div>-->
                                    <!--<h3 class="post-title2">SharkFest'17 Europe Movie</h3> 
                                    <div class="responsive-iframe">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/WyIp_c5Rax4" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    -->
                                    <h3 class="post-title2">SharkBytes</h3>
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqN6BexvXHGiGLgI99hO5ofJ" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <h3 class="post-title2">Blogs</h3>
                                    <p>
                                        <a href="https://t.co/n8DePR6oCu">Sharkfest'18 EUROPE Recap</a> by Jasper Bongertz<br>
                                        <!--<a href="https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-2-speed-duplex-and-drops/">The Network Capture Playbook Part 2</a> by Jasper Bongertz<br>
                                        <a href="https://blog.packet-foo.com/2016/11/the-network-capture-playbook-part-3-network-cards/">The Network Capture Playbook Part 3</a> by Jasper Bongertz-->
                                    </p>
                                    <h3 class="post-title2">Sponsor Videos</h3>
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/lR_AYYcuwJA" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/0H5fGzkseQQ" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    
                                    <h3 class="post-title2">Wednesday Classes</h3>
                                    <ul style="list-style:none;">
                                        <li>01: <a href="assets/presentations18eu/01-04.pptx"> Back to the Basics</a> by Hansang Bae</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://youtu.be/y13zH-8OPE8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:05)</li>
                                        </ul>
                                        
                                        <li>02: 802.11n/ac: complexity & solutions in capturing MIMO traffic by Thomas d'Otreppe</li>
                                    
                                        <li>03:  <a href="assets/presentations18eu/03.7z">Writing a Wireshark Dissector: 3 Ways to Eat Bytes</a> by Graham Bloice</li>
                                        <!--<ul>
                                            <li><a href="assets/presentations16eu/01.zip"> Presentation Materials</a></li>
                                        </ul>-->
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=IbOVR7QQkvI" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:23:38)</li>
                                        </ul>
                                        <li>04: <a href="assets/presentations18eu/01-04.pptx">Back to the Trenches</a> by Hansang Bae</li>
                                            <ul>
                                            <li class="presVideo"><a href="https://youtu.be/UlQtslfZFYA" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:12)</li>
                                            </ul>
                                    <li>05:  <a href="assets/presentations18eu/05.pdf"> Handcrafted Packets: build network packets with Scapy </a> by Uli Heilmeier</li>
                                    <ul>
                                    <li class="presVideo"><a href="https://youtu.be/vuX0KZgNT94" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (55:45)</li>
                                    </ul>
                                    <li>06: Using More of the Features of Wireshark to Write Better Dissectors by Richard Sharpe</li>
                                    <li>07: TCP - Tips, Tricks & Traces by Chris Greer</li>
                                    
                                   <!--<ul>
                                    <li class="presVideo"><a href="https://youtu.be/2XStp91GJmc"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:15:44)</li>
                                    </ul> -->
                                    <li>08: <a href="assets/presentations18eu/08.pdf">Packet Analysis in the Cloud</a> by Matthew York</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/2XStp91GJmc" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:00:31)</li>
                                    </ul>
                                    <li>09: <a href="assets/presentations18eu/09.pdf">Crash Course: IPv6 and Network Protocols</a> by Johannes Weber</li> 
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/FBfCbCB_Ui4"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:15:44)</li>
                                    </ul>
                                    <li>10:   The Unusual Suspects: open source tools for enhancing bigdata & network forensics analysis by Jasper Bongertz</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=a7q8WHTtPds" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:09)</li>
                                    </ul>
                                    <li>11: <a href="assets/presentations18eu/11-14.zip"> IoT - Buy and Install your own Destruction! (Part 1)</a> by Phill Shade</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/wa9SCzyn4qc"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:35:59)</li>
                                    </ul>

                                    <li>12: <a href="assets/presentations18eu/12.zip">Slow start and TCP Reno demystified: How congestion avoidance modes can influence a session</a> by Christian Reusch</li>
                                    <li>13: <a href="assets/presentations18eu/13.pdf">Wireshark CLI Tools & Scripting</a> by Sake Blok</li>
                                    <li>14: <a href="assets/presentations18eu/11-14.zip">IoT - Buy and Install your own Destruction!</a> (Part 2) by Phill Shade</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/wa9SCzyn4qc"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:35:59)</li>
                                    </ul>
                                    <li>15: <a href="assets/presentations18eu/15.pptx">TLS 1.2/1.3 and Data Loss Prevention</a> by Ross Bagurdes </li>
                                </ul>
                                <h3 class="post-title2">Thursday Classes</h3>
                                <ul style="list-style:none;">

                                    <li>16: SMB in the Star Wars Universe by Eddi Blenkers</li> 
                                    
                                    <li>17:  <a href="assets/presentations18eu/17.pdf">To Send or Not to Send? How TCP congestion control algorithms work</a> by Vladimir Gerasimov</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/aoHXYLuU9-I" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:23:13)</li>
                                    </ul>
                                   
                                    <li>18:  Generating Wireshark Dissectors: A status report by Richard Sharpe</li>
                                    <li>19: <a href="assets/presentations18eu/19.pdf">Hands-on Analysis of Multi-Point Captures</a> by Christian Landström</li>
                                    <li>20: <a href="assets/presentations18eu/20.zip">TCP SACK Overview and Impact on Performance</a> by John Pittle</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/B3dLe-jGyoM" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:15:44)</li>
                                    </ul>
                                    <li>21: <a href="assets/presentations18eu/21.pdf">sFlow: theory & practice of a sampling technology and its analysis with Wireshark</a> by Simone Mainardi</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/T8H1ZVvARYU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (59:53)</li>
                                    </ul>
                                    <li>22: Writing a TCP Analysis Expert System by Jasper Bongertz</li>
                                    <li>23: <a href="assets/presentations18eu/23.pdf">BGP is not only a TCP Session: learning about the protocol that holds networks together</a> by Werner Fischer</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/CXlP0SbKPFo" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:47)</li>
                                    </ul>
                                    <li>24: <a href="assets/presentations18eu/24.pptx"> Developer Bytes Lightning Talks</a> by Wireshark Core Developers</li>
                                    <li>25: <a href="assets/presentations18eu/25.pptx">Using Wireshark to Solve Real Problems for Real People: step- by-step case studies in packet analysis</a> by Kary Rogers</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/UBfSgjUCEi0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:49)</li>
                                    </ul>
                                    <li>26: <a href="assets/presentations18eu/26.pdf">Troubleshooting WLANs (Part 1): Layer 1 & 2 analysis using multi- channel hardware, Wi-Spy & Other Tools</a> by Rolf Leutert</li> 
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/dXbO-5_DkRI" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:26)</li>
                                    </ul>
                                    <li>27: <a href="assets/presentations18eu/27.zip"> IEEE802.11ac Debugging in a Windows Environment: new ways of debugging with Wireshark in a post-AirPcap era</a> by Megumi Takeshita</li>
                                    <li>28: The Packet Doctors are In! Packet trace examinations with the experts by Drs. Bae, Bongertz, Landström, Blok, and Rogers</li> 
                                    <li>29: <a href="assets/presentations18eu/29.pdf">Troubleshooting WLANs (Part 2): Using 802.11 management & control frames</a> by Rolf Leutert</li>
                                    <li>30: <a href="https://lekensteyn.nl/files/wireshark-ssl-tls-decryption-secrets-sharkfest18eu.pdf">SSL/TLS decryption: uncovering secrets</a> by Peter Wu</li>
                                </ul>
                                <h3 class="post-title2">Friday Classes</h3>
                                <ul style="list-style:none;">
                                    <li>31: <a href="https://www.dropbox.com/s/yr169s825v75vkr/Sharkfest_EU_2018.pdf?dl=0">Packet Monitoring in the Days of IoT and Cloud</a> by Luca Deri</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/irFTiNZRNj4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:32)</li>
                                    </ul>
                                    <li>32: <a href="assets/presentations18eu/16.pdf">Analyzing Kerberos with Wireshark</a> by Eddi Blenkers</li>
                                    <li>33: <a href="assets/presentations18eu/33.pptx">Reliable Packet Capture</a> by Christian Reusch || <a href="assets/presentations18eu/33.pptx">Download Presentation Materials Here</a></li>
                                    <li>34: OPEN FORUM: Aha! Moments in Packet Analysis by Chris Greer</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=_v_oE7327eQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:15:57)</li>
                                    </ul>
                                    <li>35: <a href="assets/presentations18eu/35.zip">TCP Split Brain: Compare/ Contrast TCP Effects on Client and Server with Wireshark</a> by John Pittle</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=rXH7svgisgw" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:53)</li>
                                    </ul>
                                    <li>36: Developing an Online Packet Analysis Platform by Oliver-Tobias Ripka</li>
                                </ul>
                                </div>
                            </div>
                     
                        </article> 
                    </section>



                    <!-- Content -->
                    <hr class="page-divider transparent visible-xs"/>
                    
                    <aside id="sidebar" class="sidebar col-sm-5 col-md-4 col-xs-8">
                         <div class="post-media">
                                
                            

                            <a target="_blank" href='https://photos.google.com/share/AF1QipNTKKRUQiI7ETASZayD23g8ntFPrXo91Kgqbo8ugPLptDs18g9k5MHMwFxOKeCQJA?key=MjZIZDlTbUQ3eVptbHhfdS1XQVJRajRia29hYnpB&source=ctrlq.org'><img src='assets/img/sf18eu-2.jpg' /></a>



                    </aside>

                    </div>

                    </div>


                    </section>
                     

                    <!-- /PAGE BLOG -->

                </div>
            </div>
</aside>
</div>
</div>
</section>
</div>
</div>
</section>
</div>

<?php include "footer.php"; ?>