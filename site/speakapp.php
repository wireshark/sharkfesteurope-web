<?php include "header.php"; ?>

    <!-- Content area -->
    <div class="content-area">

        <!-- PAGE BLOG -->
        <section class="page-section with-sidebar sidebar-right">
        <div class="container">
        <div class="row">

        <!-- Content -->
        <section id="content speakapp" class="content col-sm-8 col-md-8 col-centered">

            <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                <!--<div class="post-media">
                    
                    <img src="assets/img/sf08.jpg" alt=""/>
                </div>-->
                <div class="post-header">
                    <h2 class="post-title">SharkFest'21 Virtual EUROPE Speaker Page</h2>
                    <div class="post-meta">
                        
                    </div>
                </div>
                <div class="post-body">
                    <div class="post-excerpt">

                        <p>We look forward to welcoming you to <strong>SharkFest'21 Virtual EUROPE</strong> as a speaker. Please complete the form below to indicate your interest and proposed presentation subject matter.</p>

                        <h2 class="post-title2"><strong>Submission Guidelines</strong></h2>
                        <ul id="speakerList">             
                            <li>Presentations and workshops accepted for this conference are Wireshark-centric, of an educational nature, and include content that is Wireshark-specific.</li>
                            <li>Speakers are encouraged to submit sessions matching their expertise.</li>
                            <li>Presentations may be in lecture-only format, lab-based format, or any format that meets your presentation preferences and fits within the allotted presentation time frame.</li>
                            <li>Lecture-based presentations are expected to be 60 minutes long, followed by a 15 minute Q&A, typically.</li>
                            <li>Please note that duplicate subject-matter sessions are weeded out.  Should we receive multiple submissions with the same topic, we will contact the duplicate submitter(s) and ask for a revised or alternate topic.</li>              
                        </ul>
                            
                    <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScrqf9fqnrOR2nYARXn7-cuGgcW8Vfx3unefhyOvxITQu1pHw/viewform?embedded=true" width="100%" height="2784" frameborder="0" marginheight="0" marginwidth="0">Loading…</iframe>
                       
                    </div>
                </div>
                
            </article>

            <!-- About the author -->
            
            <!-- /About the author -->

            <!-- Comments -->
            
            <!-- /Comments -->

            <!-- Leave a Comment -->
            
            <!-- /Leave a Comment -->

        </section>
        <!-- Content -->

        <hr class="page-divider transparent visible-xs"/>

        <!-- Sidebar -->
        <aside id="sidebar" class="sidebar col-sm-4 col-md-3">
        
            <div class="widget categories">
            </div>

        </div>
        </div>
        </section>
        <!-- /PAGE BLOG -->

    </div>
    <!-- /Content area -->

   <?php include "footer.php"; ?>