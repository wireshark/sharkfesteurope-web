<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="container grid-container" id="main-container">
    <section id="main-content">
        <div class="col-md-10 col-sm-12 col-centered">
            <div class="retro-container">
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'21 Virtual EUROPE Retrospective</h2>
                                    <p>SharkFest’21 Virtual EUROPE, the second fully online SharkFest and first based in Europe, was populated by hundres of Wireshark enthusiasts from around the world and was a great success in educating and inspiring the assembled Wireshark community thanks to the core developers, instructors, sponsors, volunteers, staff, and generous knowledge-sharing by attendees.</p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf21veu"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'19 EUROPE Retrospective</h2>
                                    <p>SharkFest’19 Europe, the 4th Wireshark developer and user conference on the continent, was Nov 4th - 8th at the Palacio Estoril Hotel in Estoril, Portugal. Attendees traveled to share Wireshark knowledge and best practices in packet analysis with one another in a magnificent meeting space. The conference recordings, session presentation templates, blogs, and more are now available as resources for all. </p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf19eu"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'18 EUROPE Retrospective</h2>
                                    <p>SharkFest’18 Europe, the 3rd annual Wireshark developer and user conference on the continent, took place Oct 29th - Nov 2nd at the Imperial Riding School Renaissance Hotel in Vienna, Austria. Attendees traveled to share Wireshark knowledge and best practices in packet analysis with one another in a magnificent meeting space. The conference recordings, session presentation templates, blogs, and more are now available as resources for all. </p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf18eu"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12 column">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="">
                                    <div class="grid-cell">
                                        <h2>SharkFest'17 EUROPE Retrospective</h2>
                                        <p>SharkFest’17 Europe, the second annual Wireshark developer and user conference on the continent, took place November 7th - 10th at the Palacio Estoril Hotel in Portugal. Attendees traveled from 33 countries to share Wireshark knowledge and best practices in the art of packet analysis with one another in a magnificent, welcoming meeting space. The resulting conference recordings, session presentation templates, blogs, and more are now available as resources for all.</p>
                                    </div>
                                    <div class="retro-image">
                                        
                                    </div>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf17eu"><h4>Read More</h4></a>
                                </div>
                            </div>       
                        </div>
                    </div>
                    <div class="container col-md-12">
                        <div class="col-sm-6 col-xs-12 column retro-margin-bottom">
                            <div class="col-xs-12 background-blue front-column1">
                                <div class="grid-cell">
                                    <h2>SharkFest'16 EUROPE Retrospective</h2>
                                    <p>SharkFest’16 Europe, the first Wireshark developer and user conference outside of the US, was a welcome educational and networking event according to the 200+ IT professionals from 20+ countries that gathered October 17th – 19th in Arnhem, Netherlands. Conference keynote recordings, session presentations and more are now available.</p>
                                </div>
                                <div class="about-button">
                                    <a class="" href="sf16eu"><h4>Read More</h4></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
