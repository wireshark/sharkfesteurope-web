<?php include "header.php"; ?>
<div class="agenda-background-image">
</div>
<div class="row grid-row reg-row">
	<div class="col-lg-7 col-centered index-text">
	    <div class="">
	        <a class="front-button" href="assets/SharkFest22EUAgenda.pdf"><h4>Conference Agenda PDF</h4></a>
			<h4>All times are in the Western European time zone.</h4>
	    </div>
	</div>
</div>
	

<div class="container-fluid agenda-container">
	<div class="row">
		<div class="col-md-10 col-centered">
			<div class="row schedule-lvl1">
				<ul class="nav nav-pills nav-justified schedule">
					<li class="active"><a data-toggle="pill" href="#tab-zero"><h4>Day 01<br><span>31.10.2022</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-first"><h4>Day 02<br><span>1.11.2022</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-second"><h4>Day 03<br><span>2.11.2022</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-third"><h4>Day 04<br><span>3.11.2022</span></h4></a></li>
				    <li><a data-toggle="pill" href="#tab-fourth"><h4>Day 05<br><span>4.11.2022</span></h4></a></li>
				 </ul>
			</div>
			<div class="tab-content">
				<div id="tab-zero" class="table-responsive container tab-pane fade in active">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
							<li class="active"><a data-toggle="pill" href="#lvl1-tab-first"><h4>Pre-Conference Class I: Learn Wireshark! Analyzer Intro, TCP Deep Dive, and Intro to Threat Hunting</h4></a>
							<li class=""><a data-toggle="pill" href="#lvl1-tab-second"><h4>Pre-Conference Class II: Troubleshooting Voice over IP with Wireshark</h4></a></li>
						 </ul>
						 <div id="lvl1-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Check-in & Badge Pick up</h4>
											<p>Instructor: <a href="bios/chris-greer">Chris Greer</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">9:00 - 12:00</th>
										<th class="no-border">
											<h4>Class in session (with break)</h4>
											<p>Instructor: <a href="bios/chris-greer">Chris Greer</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">12:00 - 13:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">13:00 - 17:00</th>
										<th class="no-border">
											<h4>Class in session (with break)</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl1-tab-second" class="table-responsive container tab-pane fade in">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Check-in & Badge Pick up</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">9:00 - 12:00</th>
										<th class="no-border">
											<h4>Class in session (with break)</h4>
											<p>Instructor: <a href="bios/sake-blok">Sake Blok</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">12:00 - 13:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">13:00 - 17:00</th>
										<th class="no-border">
											<h4>Class in session (with break)</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-first" class="table-responsive container tab-pane fade">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule">
							<li class="active"><a data-toggle="pill" href="#lvl2-tab-first"><h4>Pre-Conference Class I: Learn Wireshark! Analyzer Intro, TCP Deep Dive, and Intro to Threat Hunting</h4></a></li>
							<li><a data-toggle="pill" href="#lvl2-tab-third"><h4>Pre-Conference Class III: Packet Capture Masterclass</h4></a></li>
							<li><a data-toggle="pill" href="#lvl2-tab-fourth"><h4>SharkFest EUROPE</h4></a></li>
						 </ul>
						 <div id="lvl2-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Check-in & Badge Pick up</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">9:00 - 12:00</th>
										<th class="no-border">
											<h4>Class in session (with break)</h4>
											<p>Instructor: <a href="bios/chris-greer">Chris Greer</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">12:00 - 13:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">13:00 - 17:00</th>
										<th class="no-border">
											<h4>Class in session (with break)</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl2-tab-third" class="table-responsive container tab-pane fade in">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">8:00 - 9:00</th>
										<th class="no-border">
											<h4>Check-in & Badge Pick up</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">9:00 - 12:00</th>
										<th class="no-border">
											<h4>Class in session (with break)</h4>
											<p>Instructor: <a href="bios/jasper-bongertz">Jasper Bongertz</a></p>
										</th>
									</tr>
									<tr>
										<th class="no-border">12:00 - 13:00</th>
										<th class="no-border">
											<h4>Lunch</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">13:00 - 17:00</th>
										<th class="no-border">
											<h4>Class in session (with break)</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl2-tab-fourth" class="table-responsive container tab-pane fade in">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th class="no-border">12:00 - 20:00</th>
										<th class="no-border">
											<h4>SharkFest'22 EUROPE Check-In & Badge Pick-Up</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">13:00 - 17:00</th>
										<th class="no-border">
											<h4>Developer Den Drop-In</h4>
										</th>
									</tr>
									<tr>
										<th class="no-border">18:00 - 20:30</th>
										<th class="no-border">
											<h4>SharkFest'22 EUROPE Welcome Dinner & Sponsor Showcase (SharkFest'22 EUROPE Attendees Only)</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-second" class="tab-pane fade">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule second-schedule">
							<li class="active"><a data-toggle="pill" href="#lvl21-tab-first"><h4>(Beginner/Intermediate) Atlantico Room</h4></a></li>
							<li><a data-toggle="pill" href="#lvl21-tab-second"><h4>(Intermediate/Advanced) Park Suite</h4></a></li>
						 </ul>
						 <div id="lvl21-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>9:00 - 10:00</th>
										<th>
											<h4 style="color: #ce8c19">Keynote: Latest Wireshark Developments & Road Map</h4>
											<p>Instructors: <a href="bios/gerald-combs">Gerald Combs</a> & Friends</p>
										</th>
									</tr>
									<tr>
										<th>10:15 - 11:30</th>
										<th>
											<h4>01: Network Troubleshooting from Scratch</h4>
											<p>Instructor: <a href="bios/jasper-bongertz">Jasper Bongertz</a></p>
										</th>
									</tr>
									<tr>
										<th>11:45 - 13:00</th>
										<th>
											<h4>03: Intro to QUIC: The TCP Killer?</h4>
											<p>Instructor: <a href="bios/chris-greer">Chris Greer</a></p>
										</th>
										</th>
									</tr> 
									<tr>
										<th>13:00 - 14:00</th>
										<th><h4>Lunch</h4></th>
									</tr>
									<tr>
										<th>14:00 - 15:15</th>
										<th>
											<h4>05: Wild PCAPs: The weird stuff is in the weeds</h4>
											<p>Instructor: <a href="">Chris Bidwell</a></p>
										</th>
									</tr>
									<tr>
										<th>15:30 - 16:45</th>
										<th>
											<h4>07: Wireshark at Enterprise Scale</h4>
											<p>Instructor: <a href="bios/stephen-donnelly.php">Dr. Stephen Donnelly</a></p>
										</th>
									</tr> 
									<tr>
										<th>17:00 - 18:15</th>
										<th>
											<h4>09: Ask the Experts: Wireshark Q&A: New Features, Feature Requests, Bug Reports</h4>
										</th>
									</tr>
									<tr>
										<th>18:30 - 20:30</th>
										<th><h4>Sponsor Technology Showcase Reception, Treasure Hunt & Dinner</h4></th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl21-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>9:00 - 10:00</th>
										<th>
											<h4 style="color: #ce8c19">Keynote: Latest Wireshark Developments & Road Map</h4>
											<p>Instructors: <a href="bios/gerald-combs">Gerald Combs</a> & Friends</p>
										</th>
									</tr>
									<tr>
										<th>10:15 - 11:30</th>
										<th>
											<h4>02: Chasing packet loss of TCP based applications using Wireshark</h4>
											<p>Instructor: <a href="bios/matthias-kaiser">Matthias Kaiser</a></p>
										</th>
									</tr>
									<tr>
										<th>11:45 - 13:00</th>
										<th>
											<h4>04: Wireshark and Mitre Attack</h4>
											<p>Instructors: <a href="bios/matteo-biscosi">Matteo Biscosi</a> & <a href="bios/marco-favilli.php">Marco Favilli</p>
										</th>
									</tr> 
									<tr>
										<th>13:00 - 14:00</th>
										<th><h4>Lunch</h4></th>
									</tr>
									<tr>
										<th>14:00 - 15:15</th>
										<th>
											<h4>06: Introduction to WiFi Security (session 1)</h4>
											<p>Instructor: <a href="bios/thomas-dotreppe">Thomas d'Otreppe</a></p>
										</th>
									</tr>
									<tr>
										<th>15:30 - 16:45</th>
										<th>
											<h4>08: Introduction to WPA Enterprise Exploitation (session 2)</h4>
											<p>Instructor: <a href="bios/thomas-dotreppe">Thomas d'Otreppe</a></p>
										</th>
									</tr> 
									<tr>
										<th>17:00 - 18:15</th>
										<th>
											<h4>10: Decrypt Kerberos/NTLM “encrypted stub data” in Wireshark</h4>
											<p>Instructor: <a href="bios/clement-notin">Clément Notin</a></p>
										</th>
									</tr>
									<tr>
										<th>18:30 - 20:30</th>
										<th><h4>Sponsor Technology Showcase Reception, Treasure Hunt & Dinner</h4></th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-third" class="tab-pane fade">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule second-schedule">
						    <li class="active"><a data-toggle="pill" href="#lvl22-tab-first"><h4>(Beginner/Intermediate) Atlantico Room</h4></a></li>
						    <li><a data-toggle="pill" href="#lvl22-tab-second"><h4>(Intermediate/Advanced) Park Suite</h4></a></li>
						 </ul>
						 <div id="lvl22-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>9:30 - 10:00</th>
										<th>
											<h4 style="color: #ce8c19">Keynote: Building the Wireshark Community </h4>
											<p>Presenter: <a href="bios/gerald-combs">Gerald Combs</a></p>
										</th>
									</tr>
									<tr>
										<th>10:15 - 11:30</th>
										<th>
											<h4>11: Contribute to Wireshark - the low hanging fruits</h4>
											<p>Instructor: <a href="bios/ulrich-heilmeier">Uli Heilmeier</a></p>
										</th>
									</tr>
									<tr>
										<th>11:45 - 13:00</th>
										<th>
											<h4>13: The Packet Doctors are in! Packet trace examinations with the experts</h4>
										</th>
									</tr> 
									<tr>
										<th>13:00 - 14:00</th>
										<th><h4>Lunch</h4></th>
									</tr>
									<tr>
										<th>14:00 - 15:15</th>
										<th>
											<h4>14: Automate your Analysis, tshark, the Swiss army knife</h4>
											<p>Instructor: <a href="bios/andre-luyer">Andre Luyer</a></p>
										</th>
									</tr>
									<tr>
										<th>15:30 - 16:45</th>
										<th>
											<h4>16: IPv6 Crash Course</h4>
											<p>Instructor: <a href="bios/johannes-weber">Johannes Weber</a></p>
										</th>
									</tr> 
									<tr>
										<th>17:00 - 18:15</th>
										<th>
											<h4>18: TopN analysis using Wireshark</h4>
											<p>Instructor: <a href="bios/megumi-takeshita">Megumi Takeshita</a></p>
										</th>
									</tr>
									<tr>
										<th>18:30 - 20:30</th>
										<th><h4>Sponsor Technology Showcase Reception, esPCAPe Group Packet Challenge & Dinner</h4></th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl22-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>9:30 - 10:00</th>
										<th>
											<h4 style="color: #ce8c19">Keynote: Building the Wireshark Community </h4>
											<p>Presenter: <a href="bios/gerald-combs">Gerald Combs</a></p>
										</th>
									</tr>
									<tr>
										<th>10:15 - 11:30</th>
										<th>
											<h4>12: Advanced IEC 60870-5-104 analysis with Wireshark</h4>
											<p>Instructor: <a href="#">Martin Scheu</a></p>
										</th>
									</tr>
									<tr>
										<th>11:45 - 13:00</th>
										<th>
											<h4>13: The Packet Doctors are in! Packet trace examinations with the experts</h4>
										</th>
									</tr> 
									<tr>
										<th>13:00 - 14:00</th>
										<th><h4>Lunch</h4></th>
									</tr>
									<tr>
										<th>14:00 - 15:15</th>
										<th>
											<h4>15: Spotting Hacking Attacks in a Trace File</h4>
											<p>Instructor: <a href="bios/eduard-blenkers">Eddi Blenkers</a></p>
										</th>
									</tr>
									<tr>
										<th>15:30 - 16:45</th>
										<th>
											<h4>17: Security Monitoring for SMBs</h4>
											<p>Instructor: <a href="bios/christian-landstrom">Christian Landström</a></p>
										</th>
									</tr> 
									<tr>
										<th>17:00 - 18:15</th>
										<th>
											<h4>19: LOG4SHELL: Getting to know your adversaries</h4>
											<p>Instructor: <a href="bios/sake-blok">Sake Blok</a></p>
										</th>
									</tr>
									<tr>
										<th>18:30 - 20:30</th>
										<th><h4>Sponsor Technology Showcase Reception, esPCAPe Group Packet Challenge & Dinner</h4></th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div id="tab-fourth" class="tab-pane fade">
					<div class="tab-content lvl2">
						<ul class="nav nav-pills nav-justified schedule second-schedule">
							<li class="active"><a data-toggle="pill" href="#lvl23-tab-first"><h4>(Beginner/Intermediate) Atlantico Room</h4></a></li>
						    <li><a data-toggle="pill" href="#lvl23-tab-second"><h4>(Intermediate/Advanced) Park Suite</h4></a></li>
						 </ul> 
						 <div id="lvl23-tab-first" class="table-responsive container tab-pane fade in active">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>9:00 - 10:00</th>
										<th>
											<h4>SHARKBYTES</h4>
											<p>Instructors: <a href="bios/gerald-combs">Gerald Combs</a> (Founder, Wireshark; Director of Open Source Projects, Sysdig) & Loris Degionanni (CTO and Founder, Sysdig)</p>
										</th>
									</tr>
									<tr>
										<th>10:15 - 11:30</th>
										<th>
											<h4>20: Understanding TCP Throughput</h4>
											<p>Instructor: <a href="bios/kary-rogers">Kary Roger</a></p>
										</th>
									</tr>
									<tr>
										<th>11:45 - 13:00</th>
										<th>
											<h4>22: Wireshark with LTE and 5G Packet Core </h4>
											<p>Instructor: <a href="bios/mark-stout">Mark Stout</a></p>
										</th>
									</tr>
									<tr>
										<th>13:00 - 14:00</th>
										<th>
											<h4>A walkthrough of the SharkFest esPCAPe & CTF Challenges</h4>
										</th>
									</tr>
									<tr>
										<th>14:00 - 15:00</th>
										<th>
											<h4>Closing Remarks, Challenge Awards & Farewell Reception</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
						<div id="lvl23-tab-second" class="table-responsive container tab-pane fade">
							<table class="schedule-table table">
								<tbody>
									<tr>
										<th>9:00 - 10:00</th>
										<th>
											<h4>SHARKBYTES</h4>
										</th>
									</tr>
									<tr>
										<th>10:15 - 11:30</th>
										<th>
											<h4>21: Learning Bluetooth Low Energy with Wireshark</h4>
											<p>Instructor: <a href="#">Ville Haapakangas</a></p>
										</th>
									</tr>
									<tr>
										<th>11:45 - 13:00</th>
										<th>
											<h4>23: Visualizing and Decrypting TLS 1.3</h4>
											<p>Instructor: <a href="bios/ross-bagurdes">Ross Bagurdes</a></p>
										</th>
									</tr>
									<tr>
										<th>13:00 - 14:00</th>
										<th>
											<h4>A walkthrough of the SharkFest esPCAPe & CTF Challenges</h4>
										</th>
									</tr>
									<tr>
										<th>14:00 - 15:00</th>
										<th>
											<h4>Closing Remarks, Challenge Awards & Farewell Reception</h4>
										</th>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include  "footer.php"; ?>