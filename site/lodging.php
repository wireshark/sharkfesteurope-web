<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>
<div class="lodging-page-img">
</div>
<div class="container">
	<div class="row lodging-page">
		<div class="col-sm-10 col-centered about-container about-text">			
			<h2><a target="_blank" href="https://www.palacioestorilhotel.com/">Palacio Estoril Hotel</a></h2>
			<p>
				Rua Particular<br>
				2769 - 504 Estoril<br>
				Portugal<br>
			</p>
			<p>Built in 1930, today´s Palácio Hotel retains many of the characteristics of that period. The ambience of exclusiveness that pervades the Hotel makes an impression on everybody who enters it, from its imposing, entirely white facade and beautiful gardens to its elegant classic decor, which has been updated over the years but without losing its combination of timeless, luxury and sophistication.</p>
			
			<h2>Rates</h2>
			<p>Single Room - €160,00</p>
			<p>Double Room - €180,00</p>
			<p>The above rates are per room, per night, including buffet breakfast and access to the Banyan Tree Spa (sauna, hammam, jacuzzi, gym and dynamic swimming-pool).</p>
			<p>City Tax - €2,00 per person/day</p>
			
			<h4>To book your room for SharkFest'22 EUROPE:</h4>
			<ul>
				<li>Go to <a target="_blank" href="https://www.palacioestorilhotel.com/">https://palacioestorilhotel.com/</a></li>
				<li>Click on <b>Book Now</b> at the top far right of the hotel homepage</li>
				<li>Select your dates</li>
				<li>In the Promotional Code box, please put in code: <b>SHARKFEST2022</b></li>
			</ul>
			<p><b>Please note</b>: this promotional code is good for the nights of 30th October to the 5th November 2022. If you need to stay an extra night(s), please email <a href="mailto:sheri@wireshark.org">sheri@wireshark.org</a> and we will do our best to arrange it for you. If you run into any issues, please email us and we'll put you in touch with the hotel.</p>
			
			<h2>Local Hotel Options</h2>
			<p><strong>Walking Distance:</strong></p>
			<ul>
				<li>Vila Galé Estoril (4*) – right next door <br>phone number: +351-214 648 400 <br><a href="mailto:estoril.reservas@vilagale.com">estoril.reservas@vilagale.com</a></li>
				<li>Hotel Inglaterra Charme & Boutique (4*) <br>   phone number: +351-214 684 461</li>
				<li>Hotel Alvorada (3*)  <br>  phone number: +351-214 649 860</li>
			</ul>
			<p><strong>Near but not walking distance:</strong></p>
			<ul>
				<li>InterContinental Estoril (5*) <br> phone number: +351-218 291 100 <br><a href="mailto:liset.reservations@ihg.com"> liset.reservations@ihg.com</a></li>
				<li>Estoril Eden (4*) <br> phone number: +351-214 667 600<br><a href="mailto:reservas@hotelestorileden.pt">  reservas@hotelestorileden.pt</a></li>
				<li>Amazonia Estoril Hotel (4*)</li>
				<li>Hotel Londres (3*)</li>
			</ul>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>