<?php
 


if(isset($_POST['email'])) {
 
     
 
    // EDIT THE 2 LINES BELOW AS REQUIRED
 
    $email_to = "spampinatodesign@gmail.com, janice.spampinato@gmail.com, sharkfest@wireshark.org";
 
    $email_subject = "SHARKFEST EU Speaker Form Submission ";
 
     
 
     
 
    function died($error) {
 
        // your error code can go here
 
        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
 
        echo "These errors appear below.<br /><br />";
 
        echo $error."<br /><br />";
 
        echo "Please go back and fix these errors.<br /><br />";
 
        die();
 
    }
 
     
 
    // validation expected data exists
 
    if(!isset($_POST['fName']) ||
 
        !isset($_POST['lName']) ||
 
        !isset($_POST['email']) ||
 
        !isset($_POST['cell']) ||
 
        !isset($_POST['company']) ||

        !isset($_POST['title']) ||

        !isset($_POST['address']) ||

        !isset($_POST['address2']) ||

        !isset($_POST['city']) ||

        !isset($_POST['state']) ||

        !isset($_POST['zip']) ||

        !isset($_POST['shirt']) ||


        !isset($_POST['bio']) ||

        !isset($_POST['preTitle']) ||

        !isset($_POST['subTitle']) ||

        !isset($_POST['format']) ||

        !isset($_POST['abstract']) ||

        !isset($_POST['audience']) ||

        !isset($_POST['additional']))

    {
 
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
 
    }
 
     
 
    $fName = $_POST['fName']; // required
 
    $lName = $_POST['lName']; // required
 
    $email = $_POST['email']; // required
 
    $cell = $_POST['cell']; // required
 
    $company = $_POST['company']; // required

    $title = $_POST['title']; // required

    $address = $_POST['address']; // required

    $address2 = $_POST['address2']; // not required

    $city = $_POST['city']; // required

    $state = $_POST['state']; // required

    $zip = $_POST['zip']; // required

    $shirt = $_POST['shirt']; // required

    

    $bio = $_POST['bio']; // required

    $preTitle = $_POST['preTitle']; // required

    $subTitle = $_POST['subTitle']; // not required

    $format = $_POST['format']; // required

    $abstract = $_POST['abstract']; // required

    $audience = $_POST['audience']; // required

    $additional = $_POST['additional']; // not required
 
     
 
    $error_message = "";
 
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email)) {
 
    $error_message .= 'The Email Address you entered does not appear to be valid.<br />';
 
  }
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$fName)) {
 
    $error_message .= 'The First Name you entered does not appear to be valid.<br />';
 
  }
 
  if(!preg_match($string_exp,$lName)) {
 
    $error_message .= 'The Last Name you entered does not appear to be valid.<br />';
 
  }

    $string_exp = "/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/";
  
  if(preg_match($string_exp,$cell)) {
 
    $error_message .= 'The cell phone number you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$company)) {
 
    $error_message .= 'The company you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$title)) {
 
    $error_message .= 'The job title you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$address)) {
 
    $error_message .= 'The address you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$city)) {
 
    $error_message .= 'The city you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$state)) {
 
    $error_message .= 'The state/province you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$zip)) {
 
    $error_message .= 'The postal code you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$shirt)) {
 
    $error_message .= 'The shirt size you entered does not appear to be valid.<br />';
 
  }

  if(preg_match($string_exp,$bio)) {
 
    $error_message .= 'Please enter your biography.<br />';
 
  }

   if(preg_match($string_exp,$preTitle)) {
 
    $error_message .= 'Please enter the title of your presentation.<br />';
 
  }

   if(preg_match($string_exp,$format)) {
 
    $error_message .= 'Please enter the format of your presentation.<br />';
 
  }

   if(preg_match($string_exp,$abstract)) {
 
    $error_message .= 'Please enter the abstract.<br />';
 
  }

   if(preg_match($string_exp,$audience)) {
 
    $error_message .= 'Please enter the audience level of your presentation.<br />';
 
  }

  if(strlen($error_message) > 0) {
 
    died($error_message);
 
  }
 
    $email_message = "";
 
     
 
    function clean_string($string) {
 
      $bad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($bad,"",$string);
 
    }
 
     
 
    $email_message .= "First Name: ".clean_string($fName)."\n";
 
    $email_message .= "Last Name: ".clean_string($lName)."\n";
 
    $email_message .= "Email: ".clean_string($email)."\n";
 
    $email_message .= "Cell Phone: ".clean_string($cell)."\n";
 
    $email_message .= "Company: ".clean_string($company)."\n";

    $email_message .= "Title: ".clean_string($title)."\n";

    $email_message .= "Address: ".clean_string($address)."\n";

    $email_message .= "Address2: ".clean_string($address2)."\n";

    $email_message .= "City: ".clean_string($city)."\n";

    $email_message .= "State: ".clean_string($state)."\n";

    $email_message .= "Zip: ".clean_string($zip)."\n";

    $email_message .= "Shirt Size: ".clean_string($shirt)."\n";

    $email_message .= "Bio: ".clean_string($bio)."\n";

    $email_message .= "Presentation Title: ".clean_string($preTitle)."\n";

    $email_message .= "Subtitle: ".clean_string($subTitle)."\n";

    $email_message .= "Format: ".clean_string($format)."\n";

    $email_message .= "Abstract: ".clean_string($abstract)."\n";

    $email_message .= "Audience Level: ".clean_string($audience)."\n";

    $email_message .= "Additional Information: ".clean_string($additional)."\n";
     
 
     
 
// create email headers
 
$headers = 'From: sharkfest-do-not-reply@wireshark.org'.$email_from."\r\n".
 
'Reply-To: '.$email_from."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($email_to, $email_subject, $email_message, $headers);  
 
// outputting to CSV




?>
 


 
 
<!-- include your own success html here -->
 
 
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SharkFest™ Europe</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="css/animate.min.css" rel="stylesheet" type="text/css"> 
    <link href="css/lightbox.css" rel="stylesheet" type="text/css"> 
    <link href="css/main.css" rel="stylesheet" type="text/css">
    <link href="css/responsive.css" rel="stylesheet" type="text/css">
    <link href="css/scrolling-nav.css" rel="stylesheet" type="text/css">
    <link href="custom.css" rel="stylesheet">


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,600,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700,200' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]      
    <link rel="shortcut icon" href="">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">-->
</head><!--/head-->

<body>

    <?php include($_SERVER['DOCUMENT_ROOT'] . "/header.html"); ?>
    <!--/#header-->

   <!-- Wrap all content -->
<div class="wrapper" style="margin-top:px;">

   

            
    <!-- /HEADER -->

    <!-- Content area -->
    
    <div class="content-area">

        <!-- PAGE BLOG -->
        <section class="services">
        <div class="container" id="content2">
        <div class="row">

        <!-- Content -->
        <section id="content" class="content col-sm-12 col-md-99">

<div class="post-header" style="margin-top: -40px">
                    <h2 class="post-title2"><strong>SharkFest '16 Europe Speaker Page</strong></h2>
</div>
</section>
                    <p>Thank you for filling out the SharkFest speaker application form! We will be in touch with you soon.</p>

        


        <!-- /PAGE BLOG -->

    </div>
</div>
</section>
</div>
</div>

    
     <!-- FOOTER -->
    <?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.html"); ?>
    
    
    <!-- /FOOTER -->
   
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>

    <script type="text/javascript" src="js/main.js"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="js/scrolling-nav.js"></script>
  
</body>
</html>



 
 
 
<?php
 
}
 
include $_SERVER['DOCUMENT_ROOT']."success.html";
?>
