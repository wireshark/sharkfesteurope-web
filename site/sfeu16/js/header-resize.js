$(function(){
    $('.navbar').data('size','big');
});

$(window).scroll(function(){
    if($(document).scrollTop() > 0)
    {
        if($('.navbar').data('size') == 'big')
        {
            $('.navbar').data('size','small');
            $('.navbar').stop().animate({
                height:'50px'
            },600);
        }
    }
    else
    {
        if($('.navbar').data('size') == 'small')
        {
            $('.navbar').data('size','big');
            $('.navbar').stop().animate({
                height:'100px'
            },600);
        }  
    }
});

$(window).on("scroll touchmove", function () {
    $('.navbar').toggleClass('tiny', $(document).scrollTop() > 0);
});