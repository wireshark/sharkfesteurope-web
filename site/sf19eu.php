<?php include "header.php"; ?>
   <!-- Wrap all content -->
   <div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf19eu-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'19 EUROPE Retrospective</h1>
            <p>November 4th - 8th, 2019<br>Palacio Estoril Hotel | Estoril, Portugal</p>
        </div>
    </div>
    <section id="main-content">
            <div class="wrapper retrospective-page"">
                <!-- /HEADER -->
                <!-- Content area -->
                <div class="content-area">

                    <!-- PAGE BLOG -->
                    <section class="services">
                    <div class="container" id="content2">
                    <div class="row">

                    <!-- Content -->
                    <section id="content5" class="content col-sm-7 col-md-8 col-lg-8">

                        <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                           <div class="post-media retro-image">
                                
                            </div>
                            <div class="post-header">
                            </div>
                            <div class="post-body">
                                <div class="post-excerpt">
                                    <p></p>     
                                    <h3 class="post-title2">Keynote Presentation</h3>
                                    <p></p>
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/9azl2wxnSoE" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <!--<h3 class="post-title2">SharkFest'17 Europe Movie</h3> 
                                    <div class="responsive-iframe">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/WyIp_c5Rax4" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    -->
                                    <!-- <h3 class="post-title2">SharkBytes</h3>
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqN6BexvXHGiGLgI99hO5ofJ" frameborder="0" allowfullscreen></iframe>
                                    </div> -->
                                    
                                    <h3 class="post-title2">Wednesday Classes</h3>
                                    <ul style="list-style:none;">
                                        <li>01: Back to the Packet Trenches by Hansang Bae</li>
                                        <!-- <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=N_Ybv7EfAk8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:05)</li>
                                        </ul> -->
                                        
                                        <li>02: <a href="assets/presentations19eu/02.pdf">Troubleshooting WLANs (Part 1): Layer 1 & 2 analysis using multi-channel hardware, Wi-Spy & other tools</a> by Rolf Leutert</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=N_Ybv7EfAk8" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:32)</li>
                                        </ul>
                                        <li>03: <a href="assets/presentations19eu/03.zip">Writing a Wireshark Dissector: 3 Ways to Eat Bytes</a> by Graham Bloice</li>
                                        <li>04: <a href="assets/presentations19eu/04.pptx">How Long is a Packet?</a> by Stephen Donnelly</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://youtu.be/1Rbx_LRPABM" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (56:27)</li>
                                        </ul>
                                    <li>05: <a href="assets/presentations19eu/05.pdf">Troubleshooting WLANs (Part 2): Layer 1 & 2 analysis using multi-channel hardware, Wi-Spy & other tools by Rolf Leutert</a></li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=Ku68vxdowQQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:35)</li>
                                    </ul>
                                    <li>06: Creating dissectors like apro by generating dissectors by Richard Sharpe</li>
                                    <li>07: Solving (SharkFest) packet challenges using tshark alone by Sake Blok</li>
                                    <ul>
                                    <li class="presVideo"><a href="https://www.youtube.com/watch?v=PaUX5YT7uj0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:11:19)</li>
                                    </ul>
                                   <!--<ul>
                                    <li class="presVideo"><a href="https://youtu.be/2XStp91GJmc"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:15:44)</li>
                                    </ul> -->
                                    <li>08: Audio & Video with Wireshark by Megumi Takeshita [<a href="http://www.ikeriri.ne.jp/wireshark/sf19eu-ikeriri-traces.zip">Traces</a>] [<a href="http://www.ikeriri.ne.jp/wireshark/sf19eu-ikeriri-tools.zip">Tools</a>]</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=JTdbQSm5MXo" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:22)</li>
                                    </ul>
                                    <li>09: Schrödinger‘s packets: they lie as long as they don’t lie by Uli Heilmeier</li> 
                                    <li>10: Tracing Uncharted Networks by Eddi Blenkers</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=x_Jj-abSzM4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (53:39)</li>
                                    </ul>
                                    <li>11: <a href="assets/presentations19eu/11.pdf">TCP Split Brain: Compare/contrast TCP effects on client & server with Wireshark (Part 1)</a> by John Pittle</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=BMtH9WiKxc8"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:08:59)</li>
                                    </ul>

                                    <li>12: BACnet & Wireshark for beginners by Werner Fischer</li>
                                    <li>13: <a href="https://www.ntop.org/wp-content/uploads/2019/11/Sharkfest_EU_2019.pdf">Packet-less traffic analysis using Wireshark</a> by Luca Deri & Samuele Sabella</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=6N72dSXdPJY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:44)</li>
                                    </ul>
                                    <li>14: <a href="assets/presentations19eu/14.pdf">TCP Split Brain: Compare/contrast TCP effects on client & server with Wireshark (Part 2)</a> by John Pittle</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=gOcP_pWkWsU&t=4308s" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:22:21)</li>
                                    </ul>
                                    <li>15: TLS 1.2/1.3 and Data Loss Prevention by Ross Bagurdes </li>
                                </ul>
                                <h3 class="post-title2">Thursday Classes</h3>
                                <ul style="list-style:none;">

                                    <li>16: War story: troubleshooting issues on encrypted links by Christian Landström</li> 
                                    
                                    <li>17: <a href="assets/presentations19eu/17.pptx">Wireshark as a part of your DevSecOps Cycle</a> by Milorad Imbra</li>
                                   
                                    <li>18: Reliable Packet Capture by Christian Reusch</li>
                                    <li>19: EXPERT PANEL: Pros & Cons of Building your own Capture Appliance by Sake Blok, Jasper Bongertz, Hansang Bae, Luca Deri, and Chris Greer</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=fmVMkyQqDzg" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:18)</li>
                                    </ul>
                                    <li>20: <a href="assets/presentations19eu/20.pdf">Automate your analysis: tshark, the Swiss army knife</a> by Andre Luyer</li>
                                    <li>21: Analysis & Troubleshooting of IPsec VPNs by Jean-Paul Archier</li>
                                    <li>22: Using Wireshark to solve real problems for real people: step-by-step case studies in packet analysis by Kary Rogers</li>
                                    <li>23: Is it the network? (Part 1) by Matthias Kaiser</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=eJlpz66r2yI" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:22)</li>
                                    </ul>
                                    <li>24: IPv6 Crash Course:Understanding IPv6 as seenon the wire by Johannes Weber</li>
                                    <li>25: The Packet Doctors are In! by Drs. Bae, Blok, Bongertz, Landström & Rogers</li>
                                    <li>26: Is it the network? (Part 2) by Matthias Kaiser</li> 
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=dDv1xF_MqS4" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:25:42)</li>
                                    </ul>
                                    <li>27: WiFi Security 101 (Part 1 by Thomas D'Otreppe</li>
                                    <li>28: Case studies of a cloud-based packet analysis and learning platform by Oliver-Tobias Ripka</li> 
                                    <li>29: Troubleshooting Cisco Software-Defined Access architectures with Wireshark by Josh Halley</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=G7lbAsq87pw" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:07)</li>
                                    </ul>
                                    <li>30:  WiFi Security 101 (Part 2 by Thomas D'Otreppe</li>
                                </ul>
                                <h3 class="post-title2">Friday Classes</h3>
                                <ul style="list-style:none;">
                                    <li>31: Gentlemen’s Software Set for Transport Protocols Testing & Learning by Vladimir Gerasimov</li>
                                    <li>32: <a href="https://www.ntop.org/wp-content/uploads/2019/11/Plaintext-Information-In-Network-Protocols-2.pdf">Green Locks are not enough: Plaintext information in network protocols</a> by Simone Mainardi</li>
                                    <li>33: <a href="assets/presentations19eu/33.pdf">Analysing VoIP Protocols: Discover Wireshark’s numerous features to troubleshoot VoIP</a> by Rolf Leutert</li>
                                    <li>34: LTE Explained...The Other Protocols by Mark Stout</li>
                                    <li>35: <a href="https://lekensteyn.nl/files/wireshark-tls-debugging-sharkfest19eu.pdf">Debugging TLS issues with Wireshark</a> by Peter Wu</li>
                                    <li>36: Automating cloud infrastructure for packet capture and analysis by Brad Palm & Ryan Richter</li>
                                </ul>
                                </div>
                            </div>
                     
                        </article> 
                    </section>



                    <!-- Content -->
                    <hr class="page-divider transparent visible-xs"/>
                    
                    <aside id="sidebar" class="sidebar col-sm-5 col-md-4 col-xs-8">
                        <h3>Photo Gallery</h3>
                         <div class="post-media">
                            <a target="_blank" href='https://photos.app.goo.gl/M9uuv7eFyuF2FuxA8'><img src='assets/img/sf19eu-3.jpg' /></a>
                    </aside>
                    </div>
                    </div>
                    </section>
                    <!-- /PAGE BLOG -->
                </div>
            </div>
</aside>
</div>
</div>
</section>
</div>
</div>
</section>
</div>

<?php include "footer.php"; ?>