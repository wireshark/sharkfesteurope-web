<!DOCTYPE html>
<html class="full" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SharkFest™ EUROPE</title>

    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/css/full.css" rel="stylesheet">

    <!-- Oswald Font -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

    <link href="/css/lightbox.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel='shortcut icon' href='/img/favicon.ico' type='image/x-icon'/ >
</head>
<body>
        <!-- Navigation -->
    <header class="header">
        <nav class="navbar navbar-default " role="navigation">
            <div class="container" id="ninety-nine-width">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="/index">                      
                        <img class="header-logo" style="" src="../img/sf22eu/sf22eu-logo.png">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-right bigger-collapse " id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Attend<b class="caret"></b></a>
                             <ul class="dropdown-menu">
                                <li><a href="/about">About</a></li>
                                <li><a href="/agenda">Agenda</a></li>
                                <li><a href="/lodging">Lodging</a></li>
                                <li><a href="/covid">COVID</a></li>
                                <li><a target="_blank" href="https://forms.gle/MjmTSJrj9ARsiA8t9">Women in Tech Scholarship</a></li>
                                <!-- <li><a href="/register">Register</a></li> -->
                                <li><a href="/about#why-attend">Why Attend</a></li>
                                <li><a href="/first-time">First Time Attendees</a></li>
                             </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Sponsor <b class="caret"></b></a>
                             <ul class="dropdown-menu">
                                <li><a href="/sponsors">Sponsors</a></li>
                                <li><a href="/sponsors">Become a Sponsor</a></li>                                
                             </ul>
                        </li>
                        <li>
                            <a href="/retrospective">Retrospective</a>
                        </li>
                        <li>
                            <a href="/shop">Shop</a>
                        </li>
                        <!-- <li>
                            <a target="_blank" rel="noopenner noreferrer" href="https://pretalx.packet-foo.com/sharkfest-22-eu-2022/cfp">Speaker Submission</a>
                        </li> -->
                        <!-- <li>
                            <div class="header-button-front">
                                <a class="header-button" href="/register"><h4>Register Now</h4></a>
                            </div>
                        </li> -->
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
        <h6 id="front-modal-button" class="">Sign up for our mailing list!</h6>
        <div id="front-modal" class="front-modal-wrapper">
                <span class="close">&times;</span>
                <div class="front-modal-content">
                    <!-- Begin Mailchimp Signup Form -->
                    <link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
                    <style type="text/css">
                        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                        /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                        We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                    </style>
                    <style type="text/css">
                        #mc-embedded-subscribe-form input[type=checkbox]{display: inline; width: auto;margin-right: 10px;}
                        #mergeRow-gdpr {margin-top: 20px;}
                        #mergeRow-gdpr fieldset label {font-weight: normal;}
                        #mc-embedded-subscribe-form .mc_fieldset{border:none;min-height: 0px;padding-bottom:0px;}
                    </style>
                    <div id="mc_embed_signup">
                        <form action="https://wireshark.us16.list-manage.com/subscribe/post?u=9a409f05fc21d8b00f86d4254&amp;id=6135358745" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div id="mc_embed_signup_scroll">
                            <label for="mce-EMAIL">Subscribe to SharkFest</label>
                            <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                        <div id="mergeRow-gdpr" class="mergeRow gdpr-mergeRow content__gdprBlock mc-field-group">
                            <div class="content__gdpr">
                                <label>Marketing Permission</label>
                                <fieldset class="mc_fieldset gdprRequired mc-field-group" name="interestgroup_field">
                                <label class="checkbox subfield" for="gdpr_38875"><input type="checkbox" id="gdpr_38875" name="gdpr[38875]" value="Y" class="av-checkbox "><span>Sysdig, Inc. sponsors Wireshark and manages its community resources. By clicking Subscribe, you acknowledge that your data will be subject to the <a href="https://sysdig.com/privacy-policy/">Sysdig Privacy Policy</a>. In order to ensure continuous service, your data may be transferred to future sponsors of the Wireshark community.  Wireshark, and Wireshark sponsors, will only use your data to provide you with communications you request.  You can unsubscribe at any time by clicking the link in the footer of our emails.</span> </label>
                                </fieldset>
                            </div>
                            <div class="content__gdprLegal">
                                <p>We use MailChimp as our marketing platform. By clicking below to subscribe, you acknowledge that your information will be transferred to MailChimp for processing. <a href="https://mailchimp.com/legal/" target="_blank">Learn more about MailChimp's privacy practices here.</a></p>
                            </div>
                        </div>
                            <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9a409f05fc21d8b00f86d4254_6135358745" tabindex="-1" value=""></div>
                            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                            </div>
                        </form>
                    </div>
                    <!--End mc_embed_signup-->
                </div>
            </div>
    </header>
