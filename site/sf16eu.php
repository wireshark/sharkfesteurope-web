<?php include "header.php"; ?>
   <!-- Wrap all content -->
 <div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf16eu-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'16 EUROPE Retrospective</h1>
            <p>October 17-19<br>Papendal Hotel | Arnhem, the Netherlands</p>
        </div>
    </div>
    <section id="main-content">
        <div class="wrapper retrospective-page">
             <!-- /HEADER -->
            <!-- Content area -->
            <div class="content-area">

                <!-- PAGE BLOG -->
                <section class="services">
                <div class="container" id="content2">
                <div class="row">

                <!-- Content -->
                <section id="content5" class="content col-sm-8 col-md-9 col-lg-8">

                    <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                       <div class="post-media retro-image">
                        </div>
                        <div class="post-header">
                            <h3 class=""></h3>
                        </div>
                        <div class="post-body">
                            <div class="post-excerpt">    
                                <h3 class="post-title2"><strong>Keynote Presentation</strong></h3>
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/K7vb8YoeEyA" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <p><strong>Wireshark Behind The Scenes</strong><br/>
                                Gerald Combs</p>
                                
                                <h3 class="post-title2"><strong>Blogs</strong></h3>
                                <p><a href="https://blog.packet-foo.com/2016/10/sharkfest-europe-2016-retrospective/">SharkFest '16 Europe Recap</a> by Jasper Bongertz <br>
                                    <a href="https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-1-ethernet-basics/">The Network Capture Playbook Part 1</a> by Jasper Bongertz<br>
                                    <a href="https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-2-speed-duplex-and-drops/">The Network Capture Playbook Part 2</a> by Jasper Bongertz<br>
                                    <a href="https://blog.packet-foo.com/2016/11/the-network-capture-playbook-part-3-network-cards/">The Network Capture Playbook Part 3</a> by Jasper Bongertz
                                </p>             
                                
                                <h3 class="post-title2"><strong>Tuesday Classes</strong></h3>
                                <ul style="list-style:none;">
                                    <li>01: <a href="assets/presentations16eu/01.pptx">In the Packet Trenches (Part 1)</a> by Hansang Bae</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/Gs6Ogvh7uKY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:32)</li>
                                    </ul>
                                    <ul>
                                        <li><a href="assets/presentations16eu/01.zip"> Presentation Materials</a></li>
                                    </ul>
                                    <li>02:<a href="assets/presentations16eu/02.pdf">  Scaling Up: How to Profitably use Wireshark for Analyzing Large Traces & High-Speed Network Links</a> by Luca Deri</li>
                                
                                    <li>03: <a href="assets/presentations16eu/01.pptx"> In the Packet Trenches (Part 2)</a> by Hansang Bae</li>
                                    <ul>
                                        <li><a href="assets/presentations16eu/01.zip"> Presentation Materials</a></li>
                                    </ul>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/Gs6Ogvh7uKY?t=51m54s" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:32)</li>
                                    </ul>
                                    <li>04: <a href="assets/presentations16eu/04.pdf">Common Packets in a Windows/Active Directory Environment: How a Windows Client Communicates in an Enterprise Network</a> by Ulrich Heilmeier</li>
                                
                  
                                <li>05: <a href="assets/presentations16eu/05.pdf"> Tackling the Haystack: How to Process Large Numbers of Packets</a> by Jasper Bongertz</li>
                                <ul>
                                <li class="presVideo"><a href="https://youtu.be/ec3UOed2knk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:19:36)</li>
                                </ul>
                                <li>06: <a href="assets/presentations16eu/06.pdf"> Troubleshooting 802.11 with Monitoring Mode: Finding Patterns in your pcap</a> by Thomas Baudelet</li>
                                <li>07: <a href="assets/presentations16eu/07.pdf">SSL troubleshooting with Wireshark (and Tshark)</a> by Sake Blok</li>
                                
                                <ul>
                                <li class="presVideo"><a href="https://youtu.be/oDaDY9QCnXk"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:15:44)</li>
                                </ul> 
                                <li>08: <a href="assets/presentations16eu/08.pdf"> Wireshark as a Spy Watermark Pen: Decrypting & Retrieving Information from Packets</a> by Megumi Takeshita</li>
                                <ul>
                                <li><a href="assets/presentations16eu/08.zip"> Presentation Materials</a></li>
                                </ul>
                                <li>09: <a href="assets/presentations16eu/09.pptx">The Little Thing Called “Micro Burst”: Why Massive Packet Loss can occur even in Low- Bandwidth-Usage Scenarios</a> by Christian Reusch</li> 
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/KbXc3c_Uboo"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (0:51:36)</li>
                                    <li><a href="assets/presentations16eu/09.zip"> Presentation Materials</a></li>
                                </ul>
                                <li>10: <a href="assets/presentations16eu/10.pdf"> Dissecting Man-on-the Side Attacks: Analysis of Active TCP Packet Injection Attacks</a> by Erik Hjelmvik</li>
                                </ul>

                                <h3 class="post-title2"><strong>Wednesday Classes</strong></h3>
                                <ul style="list-style:none;">
                                <li>11: <a href="assets/presentations16eu/11.pdf"> Forensic Network Analysis in the Time of APTs </a>by Christian Landström</li>
                                <ul>
                                <li class="presVideo"><a href="https://youtu.be/UbNqfQFQHZw" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:06)</li>
                                </ul>

                                <li>12: <a href="assets/presentations16eu/12.zip">Writing a Wireshark Dissector using WSGD, Lua & C: 3 Ways to Eat Bytes </a>by Graham Bloice</li>
                                <li>13: <a href="assets/presentations16eu/13.pptx"> Using Wireshark to Solve Real Problems for Real People: Step-by-Step Real-World Case Studies in Packet Analysis </a> by Kary Rogers</li>
                                <!--<ul>
                                <li class="presVideo"><a href="https://youtu.be/DS4j9pwVuog" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:06:13)</li>
                                </ul>-->
                                <li>14: <a href="assets/presentations16eu/14.pptx"> Viewing Snort Alerts using Wireshark: Understanding Alerts and Using the Snort Dissector to See Why & How Snort Detects Them </a> by  Martin Mathieson</li>
                                <li>15: <a href="assets/presentations16eu/15.pdf">Troubleshooting WLANs (Part 1): Layer 1 & 2 Analysis using WiSpy & AirPcap</a> by Rolf Leutert</li> 
                                <ul>
                                <li class="presVideo"><a href="https://youtu.be/do8yMqguPDk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:14:23)</li>
                                </ul>
                                <li>16:<a href="assets/presentations16eu/16.pdf"> Top 5 False Positives: Knowing When a Finding Doesn't Help </a> by Jasper Bongertz</li> 
                                <!--<ul>
                                <li class="presVideo"><a href="https://youtu.be/oDaDY9QCnXk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:09:22)</li>
                                </ul>-->
                                <li>17: <a href="assets/presentations16eu/17.pdf"> Troubleshooting WLANs (Part 2): Layer 1 & 2 Analysis using WiSpy & AirPcap </a>by Rolf Leutert</li>
                                <ul>
                                <li class="presVideo"><a href="https://youtu.be/rU-PR1p1Njc" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:01)</li>
                                </ul>
                               
                                <li>18: <a href="assets/presentations16eu/18.pdf"> Troubleshooting with Layer 2 Control Protocols </a> by Werner Fischer</li>
                                <li>19: <a href="assets/presentations16eu/19.pdf" title="Presentation slides" target="_blank"> Windows Filesharing De-Mystified: SMB with a Eureka! Effect </a> by Eduard Blenkers</li>
                                <ul>
                                <li class="presVideo"><a href="https://www.youtube.com/watch?v=lbTsiAnhfQk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:04:07)</li>
                                </ul>
                                <li>20:  <a href="assets/presentations16eu/20.pdf" title="Presentation slides" target="_blank"> Inside the Wireshark Core Developer Team: How Wireshark Development Works! </a> by  Alexis LaGoutte</li>
                                
                            </div>
                        </div>
                 
                    </article> 
                </section>



                <!-- Content -->
                <hr class="page-divider transparent visible-xs"/>
                
                <!-- Sidebar -->
                <aside id="sidebar" class="sidebar col-sm-4 col-md-3 col-xs-8">
                     <div class="post-media">
                        <a style="font-size: 25px;" data-lightbox="sf16eu" alt="Click Here to View Pictures from Sharkfest '16 Europe!" href='img/sharkfest16eugallery/2.jpg'><img src="img/sharkfest16eugallery/sfeugal.jpg" ></a></div>
                            <a href='img/sharkfest16eugallery/1.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/3.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/4.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/5.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/6.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/7.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/8.jpg' data-lightbox="sf16eu"></a>
                            
                            <a href='img/sharkfest16eugallery/9.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/10.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/11.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/12.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/13.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/14.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/15.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/16.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/17.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/18.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/19.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/20.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/21.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/22.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/23.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/24.jpg' data-lightbox="sf16eu"></a>
                            <a href='img/sharkfest16eugallery/25.jpg' data-lightbox="sf16eu"></a>
                </aside>

                </div>

                </div>


                </section>
                 

                <!-- /PAGE BLOG -->

            </div>
        </div>
    </ul>
</div>
</div>
</article>
</section>
</div>
</div>
</section>
</div>
</div>
</section>
</div>
<?php include "footer.php"; ?>