<?php include "header-small.php"; ?>
<div class="col-lg-12 agenda-header why-attend">
	<div class="col-lg-5 agenda-header why-image">
		
	</div>
	<div class="col-lg-7 agenda-header-background">
		<h3>
			<p></p>
			<a href="assets/SharkFest18EUAgenda.pdf">
				<button type="button" class="btn btn-primary btn-xl">SharkFest Conference Daily Schedule</button>
			</a>
		</h3>
		
	</div>
</div>
<div class="container-fluid why-attend why-body">
	<div class="col-lg-9">
		<h2>Why Attend?</h2>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>1</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>Thrive</strong> in a unique, immersive Wireshark educational and social networking experience.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>2</h2>
			</div>
			<div class="why-reason-text">
				<p><strong> Build diagnostic muscle </strong>by attending a selection of 36 Wireshark-centric sessions ranging from beginner to advanced/developer levels. </p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>3</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>World-class Wireshark instruction</strong> by renowned veteran packet analysts.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>4</h2>
			</div>
			<div class="why-reason-text">
				<p><strong> Meet the devs. </strong> Gerald Combs, founder of Ethereal/Wireshark open source project, and reps from the global network of core developers will be on hand to answer questions + present the Wireshark projects current and future direction.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>5</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>Laura Chappell!</strong>  Jettison your analytical skills to untold heights by attending a 2-day pre-conference troubleshooting class with one of the planet’s foremost Wireshark trainers.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>6</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>  Test your investigative chops</strong> with fellow attendees at the PacketPalooza group packet competition.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>7</h2>
			</div>
			<div class="why-reason-text">
				<p><strong> Discover Wireshark use-enhancing technology</strong> from SharkFest sponsors during technology showcase hours.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>8</h2>
			</div>
			<div class="why-reason-text">
				<p><strong> Build your professional network</strong> in a casual, social setting.</p>
			</div>
		</div>
		<div class="col-md-6 col-sm-12 why-reasons">
			<div class="round-background">
				<h2>9</h2>
			</div>
			<div class="why-reason-text">
				<p><strong>   Fun.</strong> Whether on your own or joined by significant others, revel with fellow attendees at evening social events. </p>
			</div>
		</div>
	</div>
	<div class="col-lg-3" style="display: flow-root;">
		<h2 class="orange-h2">New to SharkFest?</h2>
		<a href="first-time"><button><strong>Click here to go to our New to SharkFest page</strong></button></a>
	</div>
</div>
<!--<div class="container-fluid why-body">
	<div class="col-lg-12">
		<h2 id="faq">FAQ</h2>
		<div id="accordion" role="tablist" aria-multiselectable="true">
		  <div class="card">
		    <div class="card-header" role="tab" id="headingOne">
		      <h5 class="mb-0">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
		          Example
		        </a>
		      </h5>
		    </div>

		    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
		      <div class="card-block">
		        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		      </div>
		    </div>
		  </div>
		  <div class="card">
		    <div class="card-header" role="tab" id="headingTwo">
		      <h5 class="mb-0">
		        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
		          Example
		        </a>
		      </h5>
		    </div>
		    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
		      <div class="card-block">
		        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		      </div>
		    </div>
		  </div>
		  <div class="card">
		    <div class="card-header" role="tab" id="headingThree">
		      <h5 class="mb-0">
		        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
		          Example
		        </a>
		      </h5>
		    </div>
		    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
		      <div class="card-block">
		        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
		      </div>
		    </div>
		  </div>
		</div>
	</div>
</div>-->


<?php include  "footer.php"; ?>