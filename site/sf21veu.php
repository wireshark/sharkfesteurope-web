<?php include "header.php"; ?>
   <!-- Wrap all content -->
   <div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf21eu-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'21 Virtual EUROPE Retrospective</h1>
            <p>June 14th - 18th, 2021<br>Online</p>
        </div>
    </div>
    <section id="main-content">
            <div class="wrapper retrospective-page"">
                <!-- /HEADER -->
                <!-- Content area -->
                <div class="content-area">

                    <!-- PAGE BLOG -->
                    <section class="services">
                    <div class="container" id="content2">
                    <div class="row">

                    <!-- Content -->
                    <section id="content5" class="content col-sm-7 col-md-8 col-lg-8">

                        <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                           <div class="post-media retro-image">       
                            </div>
                            <div class="post-header">
                            </div>
                            <div class="post-body">
                                <div class="post-excerpt">
                                    <p></p>     
                                    <h3 class="post-title2">Gerald Combs Keynote: Latest Wireshark Developments & Road Map</h3>
                                    <p><a href="assets/presentations21eu/gerald-keynote.pdf">Presentation PDF</a></p>
                                    <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/2BUpoTGiDYA" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                    <h3 class="post-title2">Guillaume Valadon Keynote: Scapy Turned 18. Boy They Grow Up Fast, Don’t They!</h3> 
                                    <div class="responsive-iframe">
                                            <iframe width="560" height="315" src="https://www.youtube.com/embed/krZ3fOCTlfs" frameborder="0" allowfullscreen></iframe>
                                    </div>
                                   
                                    <h3 class="post-title2">Thursday Classes</h3>
                                    <ul style="list-style:none;">
                                        <li>01: <a href="assets/presentations21eu/01.pdf">Know your preferences</a> by Uli Heilmeier</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=l0LiNiIZjCU" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                        
                                        <li>02: <a href="assets/presentations21eu/02.pdf">Analysis and Troubleshooting of IPsec VPNs</a> by Jean-Paul Archier</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=KXVVrX_XECQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                        <li>03: <a href="assets/presentations21eu/03.pdf">Chasing application performance with Wireshark</a> by Matthias Kaiser</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=ZPoLa42Qjo8" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                        <li>04: <a href="assets/presentations21eu/04.zip">Automate your Analysis: tshark, the Swiss army knife</a> by André Luyer</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://youtu.be/1TxAq1xIj1M" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                        <li>05: <a href="assets/presentations21eu/05.pdf"> DDoS from the packet level</a> by Eddi Blenkers</a></li>
                                        <li>06: <a href="assets/presentations21eu/06.pdf">When It's NOT a "Network Problem" - Identifying Higher-Layer Issues in Packet Data</a> by Wes Morgan</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=nuLyzMZp0V8" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                        <li>07: <a href="assets/presentations21eu/07.pdf">Network Forensic Case Studies: Those Who Don't Learn from the Past Are Doomed to Repeat It</a> by Phill Shade</li>
                                        <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=MFvKkq_MEWQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                        <li>08: <a href="assets/presentations21eu/08.zip">Back to the Packet Trenches</a> by Hansang Bae</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=CT-JdEXaY64" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                        <li>09: Intro to QUIC - The TCP Killer? by Chris Greer</li> 
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=seRVM8sEf0g" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                        <li>10: <a href="assets/presentations21eu/10.pdf">Introduction to WAN Optimization</a> by John Pittle</li>
                                        <ul>
                                            <li class="presVideo"><a href="https://www.youtube.com/watch?v=jWUaVbwcSLQ" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                        </ul>
                                    </ul>
                                <h3 class="post-title2">Friday Classes</h3>
                                <ul style="list-style:none;">
                                    <li>11: How long is a packet? And does it really matter? by Dr. Stephen Donnelly</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=BMtH9WiKxc8" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                    </ul>

                                    <li>12: <a href="assets/presentations21eu/12.pptx">How to analyze SACK and DSACK with Wireshark</a> by Christian Reusch</li>
                                    <li>13: Make the bytes speak to you by Roland Knall</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=NNLvNCFhkbU" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                    </ul>
                                    <li>14: The Packet Doctors are in! Packet trace examinations with the experts by Drs. Bae, Blok, Bongertz, and Landström</li>
                                    <li>15: <a href="assets/presentations21eu/15.zip">Cybersecurity-oriented Network Traffic Analysis by Luca Deri, Matteo Biscose, and Martin Scheu </li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=YX1JG96GFlE" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                    </ul>
                                    <li>16: <a href="assets/presentations21eu/16.pdf">Dissecting WiFi6 using Wireshark by Megumi Takeshita</li> 
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/-zL-bpW0jaY" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                    </ul>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=-zL-bpW0jaY" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                    </ul>
                                    <li>17: Discovering IPv6 with Wireshark by Rolf Leutert</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=B8bNidd7Kdc" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                    </ul>
                                    <li>18: <a href="assets/presentations21eu/18.pdf">Trace Files Case Files</a> by Jasper Bongertz</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=9ZVla0uIlY0" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                    </ul>
                                    <li>19: Walk through of the creation and challenges of the CTF by Sake Blok</li>
                                    <li>20: imnurnet - Exploiting Your IPv4 Network with IPv6 by Jeff Carrell</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://www.youtube.com/watch?v=YPq4e8kn9yk" title="Presentation video on YouTube" target="_blank">Presentation Video</a></li>
                                    </ul>
                                </div>
                            </div>
                     
                        </article> 
                    </section>
                    <!-- Content -->
                    <hr class="page-divider transparent visible-xs"/>
                    </div>
                    </div>
                    </section>
                    <!-- /PAGE BLOG -->
                </div>
            </div>
</aside>
</div>
</div>
</section>
</div>
</div>
</section>
</div>

<?php include "footer.php"; ?>