<?php include "header.php"; ?>
<div class="reg-background-image">
</div>
<div class="row grid-row reg-row">
	<div class="col-lg-7 col-centered index-text">
		<h3>Don't miss out on this opportunity to learn with the best and network with your peers!</h3>
		<p>SharkFest'22 EUROPE will be a Hybrid event. While we look forward to seeing many of you in person, we realize that some of you may not be able to travel to the conference, and are happy to offer you the chance to attend virtually via Zoom. As all presentations will be live, you will be able to participate fully in the pre-conference classes and/or all conference sessions, as well as interact with presenters, core developers, sponsors and other attendees via the conference Discord channel.</p>
		<p><i>Please note: Pre-recorded sessions will only be available to conference registrants for the first 2 months after the conference.</i></p>
		<p><i>Discord access (for social interactions with the community) and session zoom links are only available to conference registrants.</i></p>
		<p><i>Please note: Attendees can change their registration from in-person to virtual and vice versa, up to October 22nd, by sending us an email to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a></i></p>
	    <!-- <div class="">
	        <a class="front-button" href="https://sharkfestregistration.wireshark.org"><h4>Register</h4></a>
	    </div> -->
	</div>
</div>
<div class="container">
	<div class="row">

		<div class="col-lg-10 col-centered reg-table ">
			<table class="table table-striped"> 
			  <tbody>
			    <tr class="thead-default">
			      <th scope="row"></th>
				  	<th>Standard [IN PERSON]</th>
			      <th>Standard [VIRTUAL]</th>
			      <th>Early Bird [IN PERSON] (through Sept 12)</th>
			      <th>Early Bird [VIRTUAL] (through Sept 12)</th>
			    </tr>  
			    <tr>
			      <th>SharkFest EUROPE Conference Only</th>
			      <td class="td-centered">€1095</td>
			      <td class="td-centered">€595</td>
				  	<td class="td-centered">€995</td>
			      <td class="td-centered">€495</td>
			    </tr>
			    <tr>
			      <th>Pre-Conference Class I: Learn Wireshark! Analyzer Intro, TCP Deep Dive, and Intro to Threat Hunting (Chris Greer • Oct 31-Nov 1)</th>
			      <td class="td-centered">€895</td>
			      <td class="td-centered">€895</td>
				  	<td class="td-centered">€845</td>
			      <td class="td-centered">€845</td>
				</tr>
				<tr>
			      <th>Pre-Conference Class II: Troubleshooting Voice over IP with Wireshark (Sake Blok • Oct 31)</th>
			      <td class="td-centered">€695</td>
			      <td class="td-centered">€695</td>
				  	<td class="td-centered">€645</td>
			      <td class="td-centered">€645</td>
				</tr>
				<tr>
			      <th>Pre-Conference Class III: Packet Capture Masterclass (Jasper Bongertz • Nov 1)</th>
			      <td class="td-centered">€695</td>
			      <td class="td-centered">€695</td>
				  	<td class="td-centered">€645</td>
			      <td class="td-centered">€645</td>
				</tr>
				<tr>
			      <th>SharkFest + Pre-Conference Class I</th>
				  	<td class="td-centered">€1990</td>
			      <td class="td-centered">€1490</td>
				  	<td class="td-centered">€1840</td>
			      <td class="td-centered">€1340</td>
				</tr>
				<tr>
			      <th>SharkFest + Pre-Conference Class II</th>
				  	<td class="td-centered">€1790</td>
			      <td class="td-centered">€1290</td>
				  	<td class="td-centered">€1640</td>
			      <td class="td-centered">€1140</td>
				</tr>
				<tr>
			      <th>SharkFest + Pre-Conference Class III</th>
				  	<td class="td-centered">€1790</td>
			      <td class="td-centered">€1290</td>
				  	<td class="td-centered">€1640</td>
			      <td class="td-centered">€1140</td>
				</tr>
				<tr>
			      <th>SharkFest + Pre-Conference Class II + Pre-Conference Class III</th>
				  	<td class="td-centered">€2485</td>
			      <td class="td-centered">€1985</td>
				  	<td class="td-centered">€2285</td>
			      <td class="td-centered">€1785</td>
				</tr>
				<tr>
			      <th>Pre-Conference Class II + Pre-Conference Class III</th>
				  	<td class="td-centered">€1390</td>
			      <td class="td-centered">€1390</td>
				  	<td class="td-centered">€1190</td>
			      <td class="td-centered">€1190</td>
				</tr>
			  </tbody>
			</table>
		</div>
		<div class="col-lg-10 col-centered">
			<div id="accordion" role="tablist" aria-multiselectable="true" class="reg-accordion">
			  <div class="card">
			    <div class="card-header" role="tab" id="headingOne">
			      <h5 class="mb-0">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			          Conference Highlights
			        </a>
			      </h5>
			    </div>

			    <div id="collapseOne" class="collapse in" role="tabpanel" aria-labelledby="headingOne">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					    <tr>
					      <th scope="row">Conference Keynotes by Industry Luminaries</th>
					    </tr>
					    <tr>
					      <th scope="row">3 Days of Instructional Sessions delivered by Wireshark Experts</th>
					    </tr>
					    <tr>
					      <th scope="row">Beginner/Intermediate/Advanced/Developer Tracks</th>
					    </tr>
					    <tr>
					      <th scope="row">Packet Challenges & Socializing</th>
					    </tr>
					    <tr>
					      <th scope="row">The Packet Doctors are In! Packet Trace Analysis with the Experts</th>
					    </tr>
					  </tbody>
					</table>
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingTwo">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			           SHARKFEST CONFERENCE DETAILS AND CANCELLATION POLICY
			        </a>
			      </h5>
			    </div>
			    <div id="collapseTwo" class="collapse in" role="tabpanel" aria-labelledby="headingTwo">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					    <tr>
					      <th scope="row">What is SharkFest?</th>
					      <th>SharkFest is a highly-interactive educational conference focused on sharing knowledge, experience and best practices among members of the Wireshark global developer and user communities.</th>
					    </tr>
					    <tr>
					      <th scope="row">Why Should I Attend?</th>
					      <th>
					      		• To benefit from an immersive Wireshark educational experience<br>
					      		• To interact with veteran packet analysts<br>
					      		• To test your Wireshark knowledge<br>
					      		• To network, socialize, and share knowledge with like-minded Wireshark enthusiasts<br>
					      		• To enhance your Wireshark user experience<br>
					      		• To meet Gerald Combs (creator of Wireshark) and some of the Wireshark Core Developers<br>
					      </th>
					    </tr>					    
					    <tr>
					      <th scope="row">Location</th>
					      <th>Estoril, Portugal & Online</th>
					    </tr>
					    <tr>
					      <th scope="row">Dates</th>
					      <th>Oct 31-Nov 4, 2022</th>
					    </tr>
					    <tr>
					      <th scope="row">Where Should I Stay?</th>
					      <th>Click <a href="lodging.php"> HERE</a> to go to the Lodging page.</th>
					    </tr>
					    <!-- <tr>
					      <th scope="row">SharkFest Registration Fee</th>
					      <th>Free</th>
					    </tr>  -->
						<tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card. POs are accepted for commercial businesses registering multiple attendees and government agencies. Payments via wire transfer will incur an additional €30 fee.</th>
					    </tr>
					    <!--<tr>
					      <th scope="row">Available SharkFest Discounts*</th>
					      <th><p>
					      		• Full-time CS students, CS faculty and IT staff of accredited educational institutions<br>
					      		• Active-duty military personnel<br>
					      		• Veterans<br>
					      		• Returning SharkFester <br><br>
					      		*Please contact sharkfest@wireshark.org for discounts and how to have them applied to your SharkFest registration fees.<br><br>
					      		
					      	  </p>
					     </th>
					    </tr>-->
					  </tbody>
					</table>
					
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <!-- <tbody>
					    <tr>
					      <td scope="row">14 days or more before the SharkFest'21 Europe Conference start date</th>
					      <td>Full Refund minus €100 Administration Fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the SharkFest'21 Europe Conference start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody> -->
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a></p>
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingThree">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
			           PRE-CONFERENCE CLASS I: Learn Wireshark! Analyzer Intro, TCP Deep Dive, and Intro to Threat Hunting
			        </a>
			      </h5>
			    </div>
			    <div id="collapseThree" class="collapse in" role="tabpanel" aria-labelledby="headingThree">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
						<tr>
					      <th scope="row">Instructor</th>
					      <th>Chris Greer</th>
					    </tr>
					    <tr>
					      <th scope="row">Date</th>
					      <th>Oct 31-Nov 1, 2022</th>
					    </tr>
					    <!-- <tr>
					      <th scope="row">Location</th>
					      <th>EOnline</th>
					    </tr>
					    <tr>
					      <th scope="row">Price</th>
					      <th>€445 for the Pre-Conference Class<br></th>
					    </tr> -->
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card.</th>
					    </tr>
					    <tr>
					      <th scope="row">Description</th>
					      <th>Wireshark can be intimidating.<br /><br />

									I remember how it felt when I first started looking at a trace file with Wireshark. Questions started flooding into my mind:<br /><br />

											What should I look for? <br />
											Where do I start? <br />
											How can I find the packets that matter? <br />
											What filters should I use? <br />
											What is "normal" and what can I ignore in all this data?<br />

									I froze under the weight of all the detail in the packets. If you have ever felt that way when looking at a pcap, this is the course for you! <br /><br />

									Throughout this course, we are going to look at real-world examples of how to practically use Wireshark to solve network problems and isolate cybersecurity incidents. This skill will help AllOps (NetOps, SecOps, DevOps) engineers to improve in their analysis and troubleshooting skills. Labs have been designed to give real-world, hands-on experience with protocols using Wireshark.<br /><br />

									While learning the art of packet analysis, we will also explore the Wireshark interface, configure custom columns, filters, and coloring rules, learning how to customize the layout so we can spot problems quickly. This course will give you comfort with the Wireshark interface and the experience you need to understand core protocols.<br /><br />

									My name is Chris Greer and I am a Wireshark University instructor, as well as a packet analysis consultant for Packet Pioneer. Like you, I started out looking at packet traces, hoping to find the right ones to solve complex issues on the network. I this course, I bring real-world examples to every lecture, exercise, and course assignment. I'm not much of a slide-deck kind of instructor. Too boring. My goal is to have your hands on the first pcap within 5 minutes of the first day and not let up until the final bell.<br /><br />

									So, you ready Packet People? Let's dig!
					      </th>
						</tr>
						<tr>
							<th scope="row">Outline</th> 
							<th>Day 1:<br /><br />

								Configuring the Wireshark Interface.<br />
								Capturing traffic with Wireshark – how and where?<br />
								Display Filters – Finding the traffic that matters<br />
								Using ICMP to troubleshoot the network<br />
								DNS Core Skills<br />
								Be the consultant – DNS gone wrong<br />
								The TCP Handshake<br /><br />

								Day 2:<br /><br />

								TCP Deep Dive – Options<br />
										SACK<br />
										Window Scaling<br />
										Troubleshooting Resets<br />
										Analyzing Retransmissions<br />
								Filtering on Conversations and Streams<br />
								Troubleshooting Low Throughput<br />
								TCP Stream Graphs<br />
								Analyzing SYN Attacks<br />
								Spotting Network Scans<br />
								Setting filters for “Weird” behavior<br />
							</th>
						</tr>
						<tr>
							<th scope="row">Who Should Attend</th> 
							<th>While this course is targeted to beginning Wireshark users, Network Engineers and Cybersecurity Professionals of all experience levels will benefit from the Wireshark tips we will share. There is something in this course for everyone in IT – and we’ll have fun while learning! </th>
						</tr>
					  </tbody>
					</table>
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the Pre-Conference Class start date</th>
					      <td>Full Refund minus €100 administrative fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the Pre-Conference Class start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a></p>
			      </div>
			    </div>
			  </div>
			  <div class="card">
			    <div class="card-header" role="tab" id="headingFour">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
			           PRE-CONFERENCE CLASS II: Troubleshooting Voice over IP with Wireshark
			        </a>
			      </h5>
			    </div>
			    <div id="collapseFour" class="collapse in" role="tabpanel" aria-labelledby="headingFour">
			      <div class="reg-p">
			        <table class="table table-striped">
								<tbody>
									<tr>
										<th scope="row">Dates</th>
										<th>Oct 31, 2022</th>
									</tr>
									<tr>
										<th scope="row">Payment Methods</th>
										<th>Payment is by credit card.</th>
									</tr>
									<tr>
										<th scope="row">Instructor</th>
										<th>Sake Blok</th>
									</tr>
									<tr>
										<th scope="row">Description</th>
										<th>Telephone systems have moved from analogue lines and ISDN to voice-over-ip (VoIP). This means the telephone calls are now being transported over our general data network. As with any networked application, things don't always go the way it was intended and this means there needs to be troubleshooting VoIP services. In this class, Sake will zoom in on SIP based VoIP systems and how to analyse VoIP calls in Wireshark. What features does Wireshark have to help analyse VoIP problems and how can WIreshark be used more efficiently when analyzing VoIP.</th>
									</tr>
									<tr>
										<th scope="row">Outline</th>
										<th>This is a lab-driven course, with plenty of hands-on, to learn:<br />
												• Creating a wireshark profile specific for VoIP analysis<br />
												• Using the Telephony menu items<br />
												• Learning about different types of VoIP systems<br />
												• Learning about the SIP, SDP, RTP and RTCP protocols</th>
									</tr>
									<tr>
										<th scope="row">Who Should Attend</th>
										<th>Wireshark is an important tool for those in the voice-over-IP field. This course is targeted toward Network/Voice Engineers with a working understanding of Wireshark who would like to increase their skills in solving VoIP problems with wireshark. Basic understanding of networking and some experience with wireshark is expected of the attendees.</th>
									</tr>
								</tbody>
							</table>
							<table class="table table-striped2 cancel-table">
								<thead class="thead-default">
									<tr>
										<th colspan="2">CANCELLATION POLICY</th>
										<th></th>					      
									</tr>
								</thead>
								<tbody>
									<tr>
										<td scope="row">14 days or more before the Pre-Conference Class start date</th>
										<td>Full Refund minus €100 administrative fee</td>
									</tr>
									<tr>
										<td scope="row">Less than 14 days before the Pre-Conference Class start date</th>
										<td>No Refund</td>
									</tr>
								</tbody>
							</table>
							<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a></p>
							</div>
				</div>
				<div class="card">
			    <div class="card-header" role="tab" id="headingFive">
			      <h5 class="mb-0">
			        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
			           PRE-CONFERENCE CLASS III: Packet Capture Masterclass
			        </a>
			      </h5>
			    </div>
			    <div id="collapseFive" class="collapse in" role="tabpanel" aria-labelledby="headingFive">
			      <div class="reg-p">
			        <table class="table table-striped">
					  <tbody>
					  	<tr>
					      <th scope="row">Instructor</th>
					      <th>Jasper Bongertz</th>
					    </tr>
					    <tr>
					      <th scope="row">Dates</th>
					      <th>Nov 1, 2022</th>
					    </tr>
					    <tr>
					      <th scope="row">Payment Methods</th>
					      <th>Payment is by credit card.</th>
					    </tr>
							<tr>
									<th scope="row">Description</th>
									<th>The Packet Analysis Master Class teaches all aspects of capturing network packets for troubleshooting, performance tuning or intrusion detection. The class will be starting with the three basic ways of capturing packets - on a local device, via SPAN port, or by using a TAP, before diving into more complex scenarios.</th>
							</tr>
							<tr>
									<th scope="row">Outline</th>
									<th>  • Local capture: easy, quick…but accurate?<br />
												• SPAN port in depth - how to use it, where it has problems<br />
												• TAPing a network link: the best precision in most situations, but not always possible<br />
												• Various capture scenarios: from home network links to multi-gigabit connections<br />
												• Capturing data in complex problem scenarios: performing multi point captures and how to work with the resulting PCAPs <br />
												• Unwanted side effects of packet captures: out-of-order time stamps, Ethernet trailers, duplicate packets and how to deal with them<br />
												• Wireless capture basics, and how to capture radio layer information with Wireshark on Windows<br />
												• TAP Tips and Tricks: what's a TAP and what isn't; why can't you solder your own Gigabit copper TAP, and how to select the one you really need?<br />
												• Going pro: special TAP devices and packet brokers<br />
												• Capture cards: Consumer-of-the-Shelf (COTS) vs. professional FPGA based cards
										</th>
							</tr>
					  </tbody>
					</table>
					<table class="table table-striped2 cancel-table">
					  <thead class="thead-default">
					    <tr>
					      <th colspan="2">CANCELLATION POLICY</th>
					      <th></th>					      
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td scope="row">14 days or more before the Pre-Conference Class start date</th>
					      <td>Full Refund minus €100 administrative fee</td>
					    </tr>
					    <tr>
					      <td scope="row">Less than 14 days before the Pre-Conference Class start date</th>
					      <td>No Refund</td>
					    </tr>
					  </tbody>
					</table>
					<p>All cancellation requests must be made in writing to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a> If registered but unable to attend, another attendee within your organization may be designated to take your place at no additional charge.  All substitution requests must be submitted by the original attendee via e-mail to <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org.</a></p>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</div>
<?php include "footer.php"; ?>