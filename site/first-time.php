<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container grid-container" id="main-container">
        <div class="first-time-image">
        </div>
<div class="row grid-row reg-row">
	<div class="col-lg-7 col-centered index-text">
	    <h3>New to SharkFest?</h3>
	    <h4>First-time attendee to-do’s and tips from past attendees to get you started.</h4>
	</div>
</div>
<div class="background-blue-aboutpage">
              <section id="why-attend" class="why-attend why-body">
                    <div class="container col-sm-12">
                        <div class="col-sm-12 col-xs-12 about-text">
                              <div class="about-container">
                                    <h3>Whether you’re a first-time SharkFester or want to make the most of your time at the conference, here’s your TO DO list:</h3>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>1</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong>Catch up on past SharkFest conferences on our <a href="retrospective">retrospective</a> page or on our <a href="https://t.co/FPJF1gXp6E?amp=1">YouTube channel</a>.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>5</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong><a href="agenda"> Review and revisit the full conference agenda</a></strong> a few times before the conference starts and create a schedule of sessions you wish to attend.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>2</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong>Follow us</strong> on <a href="https://twitter.com/WireSharkFest">Twitter</a> and <a href="https://www.facebook.com/SHARKFEST-Wireshark-Network-Analysis-Conference-51502216870/">Facebook!</a> </p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>6</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> Familiarize yourself with the most current session agenda</strong>, speaker bios, social events, last-minute event changes, and give session and instructor feedback.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>3</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> Read the title AND the abstract of SharkFest sessions of interest</strong>. Be sure the sessions you choose to attend cover the topics you’re interested in and download traces or other materials before a session start time as requested in certain session abstracts.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>7</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> Adjust your schedule on the fly</strong>. There are no requirements for attending sessions. Just show up at the assigned time. Feel free to move to a different session if it doesn't interest you. It’s your conference. Attend it your way.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>4</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> If attending virtually, please make sure you have a stable internet connection</strong>. Whether attending a hands-on session or lecture, all sessions will happen on Zoom.</p>
                                          </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12 why-reasons">
                                          <div class="round-background">
                                                <h2>8</h2>
                                          </div>
                                          <div class="why-reason-text">
                                                <p><strong> Socialize with new friends</strong>. SharkFest is an informal, social experience that brings like-minded IT Professionals together to share Wireshark knowledge. Get to know many peers and share as much as possible during your stay!</p>
                                          </div>
                                    </div>
                              </div>
                        </div>
                  </div>
              </section>
            </div>
            <div class="row reg-row" id="first-time-vids">
	            <div class="col-lg-12">
					<div class="col-md-4 text-center">
						<div class="responsive-iframe2">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/y13zH-8OPE8" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-md-4 text-center">
						<div class="responsive-iframe2">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/aM19jsUcFac" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-md-4 text-center">
						<div class="responsive-iframe2">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/UBfSgjUCEi0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>