<?php include "header.php"; ?>

<div class="container">
	<div class="row">
		<div class="col-lg-12 about-text">
			<h2>REGISTRATION PAYMENT CONFIRMATION</h2>
			<p>SharkFest’17 EUROPE Early Bird<br>
               November 7th – 10th, 2017<br>
               Palacio Estoril, Portugal 
            </p>
			<h2>Thank You!</h2>
                  <p>You’ve successfully completed the payment portion of the SharkFest’17 registration process.<p>
            <h2>PayPal Confirmation Email</h2>
                  <p>You will receive an email confirmation from Paypal indicating that your payment has been processed.</p>
            <h2>Registration Confirmation</h2>
                  <p>A separate email with SharkFest’17 EUROPE conference details will be sent within one week of payment confirmation.</p>
			<h2>Registration Details Completion Survey</h2>
                <p>A survey requesting additional registration details from each attendee will be sent before the conference start date. Conference staff require this information (shirt size, dietary and physical access restrictions, emergency contacts, e.g.) to create badges and ensure a smooth check-in process.</p>
            <h2>Overview of SharkFest’17 Europe Schedule</h2>
                <p><strong>Tuesday, November 7th</strong></p>
                <ul>
                    <li>• SharkFest Welcome Reception &amp; Dinner, 6-8:30pm</li>
                </ul>
                <p><strong>Wednesday, November 8th</strong></p>
                <ul>
                    <li>• SharkFest Sessions, 8:30am - 5:30pm</li>
                    <li>• Sponsor Technology Showcase, Reception &amp; Dinner, 6-9pm</li>
                </ul>
                <p><strong>Thursday, November 9th</strong></p>
                <ul>
                    <li>• SharkFest Sessions, 8:30am - 5:30pm</li>
                    <li>• Group Packet Competition Reception &amp; Dinner, 6-9pm</li>
                </ul>
                <p><strong>Friday, November 10th</strong></p>
                <ul>
                    <li>• SharkFest Sessions, 8:30-11:30</li>
                    <li>• Closing Remarks, Awards Ceremonies, 11:30am – Noon</li>
                    <li>• Farewell Reception, Noon – 1:30pm</li>
                </ul>
            <h2>Hotel Reservations</h2>
                <p>The Palacio Estoril is the exclusive lodging option selected for SharkFest’17 EUROPE. The conference has negotiated exceptional rates for this 5-star hotel with the intention of creating:</p>
                <ul>
                    <li>• An immersive Wireshark learning experience in a superior environment that inspires networking and socializing.</li>
                    <li>• A setting with amenities for attendees to relax and refresh while increasing their Wireshark skill set.</li>
                </ul>
            <h3>To encourage attendees to choose this lodging option, every SharkFest’17 registrant who reserves a room at the Hotel Palacio by August 1st will receive an AirPcap Nx adapter and other welcome gifts at check-in.</h3>
            <h2>Questions?</h2>
                <p>Please contact <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</p>
                <p>The SharkFest’17 EUROPE Coordinating Crew looks forward to meeting you in beautiful Estoril, Portugal in November!</p>

		</div>
	</div>
</div>


<?php include "footer.php"; ?>
