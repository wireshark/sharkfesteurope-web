<?php include($_SERVER[DOCUMENT_ROOT] . "/header.php"); ?>
<div class="text-centered pwd-protected-page">
    <section id="main-content">
        <div class="about-container">
            <h3>To get access to the SharkFest'22 Virtual EUROPE session Zoom links, please agree to the Event Terms.</h3>
            <input type="checkbox" id="agree_to_terms" name="agree_to_terms"></input>
            <label for="agree_to_terms">I agree to the <a target="_blank" href="https://www.riverbed.com/document/legal/riverbed-event-terms.pdf">Event Terms and Conditions</a>.</label>
            <a id="agenda-protected-button" class="header-button" href="/htpswd/agenda-protected"><h4>Go to Agenda</h4></a>
            <script>
                document.getElementById('agree_to_terms').addEventListener('change', function() {
                    if (this.checked) {
                        document.getElementById('agenda-protected-button').style.display = 'block'
                    } else {
                        document.getElementById('agenda-protected-button').style.display = 'none'
                    }
                })
            </script>
        </div>
    </section>
</div>
<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>