<!DOCTYPE html>
<html class="full" lang="en">
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SharkFest™ Europe</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/full.css" rel="stylesheet">

    <!-- Oswald Font -->
    <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

    <link href="css/lightbox.css" rel="stylesheet" type="text/css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel='shortcut icon' href='img/favicon.ico' type='image/x-icon'/ >
</head>


<body class="smaller-body">
    <div class="header-and-body">
        <!-- Navigation -->
    <div class="header shrink">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container" id="ninety-nine-width">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index.php">
                    <p>
                            <img class="header-logo" style="" src="img/sflogo-small.png">SharkFest'18 Europe<br><span>Oct 29th-Nov 2nd • Imperial Riding School Renaissance Hotel Vienna</span></p>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="about.php">About</a>
                        </li>
                        <li>
                            <a href="/why-attend.php">Why Attend</a>
                        </li>
                        <li>
                            <a href="agenda.php">Agenda</a>
                        </li>
                        <li>
                            <a href="/speakapp.php">Speakers</a>
                        </li>
                        <li>
                            <a href="register.php">Registration</a>
                        </li>
                        <li>
                            <a href="lodging.php">Lodging</a>
                        </li>
                        <li>
                            <a href="sponsors.php">Sponsors</a>
                        </li>
                        <!--<li>
                            <a href="sharkbytes.php">SharkBytes</a>
                        </li>-->
                        <li>
                            <a href="retrospective.php">Retrospective</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>
    </div>

    
