<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container grid-container" id="main-container">
	<section id="main-content">
		<div class="container col-sm-12 about-content about-container">

			<div class="container sharkbytes">
				<div class="row">
					<div class="col-lg-8 margin-20px">
						
						<h2>You’re invited to present a 5-minute SharkByte at Sharkfest’19 Europe!</h2>
						<p>
							Date: Thursday, November 7th, 2019<br>
							Time: 8:00am - 9:00am<br>
							Place:  Atlantico Room, Hotel Palacio Estoril, Portugal
						</p>
						<h2 class="blue-text-background">What is a SharkByte?</h2>
						<p>SharkBytes consist of “little crunchy bits of wisdom.” Like the immensely popular TED Talks, SharkBytes aim to inform, inspire, surprise and delight by delivering a speech on a personal topic in under 5 minutes.</p>
						<p>SharkBytes presentations, introduced at SharkFest’14, are a highlight of the conference.  Each year, 10 attendees offer glimpses into their lives by speaking about a particular interest, hobby, or proclivity (unrelated to Wireshark!) that reveals a side of them that fellow SharkFesters would not otherwise know.  Attendees volunteer to present a 5-minute piece through the <a href="sharkbytes-form.php">sign-up form.</a></p>
						<h2 class="blue-text-background">SharkByte Topics</h2>
						<p>SharkByte topics should:</p>
						<ul>
							<li>• require less than 5 minutes to deliver (time is strictly enforced)</li>
							<li>• be aimed at “informing, inspiring, surprising, and delighting” the audience</li>
							<li>• be based on personal or professional first-hand experiences unrelated to Wireshark</li>
						</ul>
						<p>SharkByte topics should not be:</p>
						<ul>
							<li>• political</li>
							<li>• defamatory to any person, organization or entity</li>
							<li>• used to promote, announce, or market any product</li>
						</ul>
						
					</div>
					<div class="col-lg-4 sharkbyte-image">
						<div class="">
							<img src="img/sharkbyte.png">
						</div> 
						<div class="responsive-iframe col-md-6 col-sm-6 col-lg-12">
							<iframe width="853" height="480" src="https://www.youtube.com/embed/videoseries?list=PLz_ZpPUgiXqPH_KUS074DvdLM0NSG8c8C" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="responsive-iframe col-md-6 col-sm-6 col-lg-12">
							<iframe width="853" height="480" src="https://www.youtube.com/embed/3vtZ97BNqvg" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="responsive-iframe col-md-6 col-sm-6 col-lg-12">
							<iframe width="853" height="480" src="https://www.youtube.com/embed/Q5gDOozxnY0" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
						
				</div>
			</div>
		</div>
	</section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>