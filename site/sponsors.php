<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>



<div class="text-centered">
    <section id="main-content">
        <div class="container">
            <div class="row sponsors-page about-container">
                <div class="col-sm-8 col-xs-12 background-blue">
                    <h2>Host Sponsor</h2>
                    <div class="col-lg-12 sponsor-margin-top">
                        <a target="_blank" href="https://www.sysdig.com">
                            <img src="img/sponsors/sysdig-white.png" />
                        </a>
                        <p><strong>Sysdig</strong> is driving the standard for cloud and container security. The company pioneered cloud-native runtime threat detection and response by creating Falco and Sysdig Open Source as open source standards and key building blocks of the Sysdig platform. With the platform, teams can find and prioritize software vulnerabilities, detect and respond to threats, and manage cloud configurations, permissions, and compliance. From containers and Kubernetes to cloud services, teams get a single view of risk from source to run, with no blind spots, no guesswork, no wasted time. The largest and most innovative companies around the world rely on Sysdig.
                        </p>
                    </div>
                    <h2>Angel Shark Sponsors</h2>
                    <div class="col-lg-12 sponsor-margin-top">
                        <a target="_blank" href="https://allegro-packets.com/en/ ">
                            <img src="img/sponsors/allegro-white.png" />
                        </a>
                        <p>With great support for Wireshark packet analysis, <strong>Allegro Network Multimeter</strong> enables network firefighters to conveniently identify and extract packets of interest from both Live network traffic and large pcap files. The leading-edge solutions developed by Allegro Packets, show packet-based network traffic statistics in real-time, intuitively presented according to the L2-L7 OSI structure. Network performance issues can easily be detected, by navigating the clearly structured dashboards and statistics pages. With the greatest ease, any traffic, protocol or connection can be selectively captured for last Mile analysis with Wireshark.</p>

                        <p>The product range extends from portable, great affordable adhoc analysis appliances to 4U rack-mount solutions, from 1G to 200G. German based Allegro Packets is the global pioneer for fast network analysis. Its customers include network specialists from many organizations, including large scale Enterprises and IT/VoIP service providers, across a wide range of market sectors around the world.
                        </p>
                        <a target="_blank" href="https://www.endace.com/">
                            <img src="img/sponsors/endace_big.png" />
                        </a>
                        <p><strong>Endace’s</strong> scalable, open packet-capture platform provides network-wide packet recording at speeds of 100Gbps and beyond. With fast, centralized search, powerful data-mining and the ability to integrate with, and host, security and performance monitoring solutions the open EndaceProbe platform puts definitive packet evidence right at your fingertips. Find out why Cisco, Palo Alto Networks, IBM, Splunk, Gigamon, Darktrace and many others partner with Endace to provide 100% accurate Network Recording at www.endace.com or follow Endace on Twitter, LinkedIn and Youtube.
                        </p>
                        <a target="_blank" href="https://www.profitap.com/">
                            <img src="img/sponsors/profitap.png">
                        </a>
                        <p><strong>Profitap</strong> develops and manufactures hard- and software solutions that drive network visibility and analytics on all traffic across physical and virtual infrastructures. These network visibility solutions are designed with the security, forensics, deep packet capture, and network & application performance monitoring sectors in mind.</p>

                        <p>Profitap network solutions help eliminate network downtime, add security to existing and new networks worldwide, assist in lawful interception applications and reduce network complexity. Profitap’s network monitoring tools are highly performant, secure, and user-friendly. They provide complete visibility and access to your network 24/7.</p>

                        <p>As experts in the field, Profitap products set new standards in an industry where the definition of excellence is constantly being challenged. With over 1,000 clients from 55 countries, many of which are among Fortune 500 companies, Profitap has a strong track record of trust and reliability. 
                        </p>
                        <a target="_blank" href="https://scos.nl/">
                            <img src="img/sponsors/scos.png">
                        </a>
                        <p><strong>SCOS Software<strong> is a leading Benelux supplier of Network and Cloud monitoring and management tools; specializing in Secure Managed File Transfer solutions for medium-sized to large enterprises. In addition, SCOS also supplies professional training and support for Network solutions, including Network and Forensics Analysis capabilities. Since 2010, SCOS has been the Authorized European Training Partner of Wireshark University. </p>
                        </p>
                    </div>
                    <h2>Honorary Sponsor</h2>
                    <div class="col-lg-12 sponsor-padding-bottom">
                        <a target="_blank" href="https://www.networkdatapedia.com/">
                            <img src="img/sponsors/networkdatapedia.png" />
                        </a>
                        <p>With <strong>NetworkDataPedia</strong>, formerly known as LoveMyTool, network analysts can access the latest information, tips, and tricks about network monitoring, analysis, security, performance, and management. Our primary goal, since LoveMyTool, has been to empower network technologies to succeed in what they do.  Secondly, we strive to assist businesses of all sizes in identifying issues and solutions to their technology.</p>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 column-about">
                    <div class="col-xs-12 background-blue front-column1 about-cell">
                        <img src="img/sf22eu/sponsor-doc-img.jpg">
                        <div class="grid-cell">
                            <p>View the SharkFest’22 EUROPE Sponsorship Form to read about the different sponsorship levels available. If your company would like to sponsor SharkFest, please contact <a href="mailto:sharkfest@wireshark.org">sharkfest@wireshark.org</a>.</p>
                        </div>
                        <div class="about-button">
                            <p><a class="" href="assets/sponsor-doc22.pdf"><h4>Sponsor Doc</h4></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>