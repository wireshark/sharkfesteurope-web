<?php include "header.php"; ?>
   <!-- Wrap all content -->

   <div class="container grid-container" id="main-container">
    <!-- Content area -->
    <div class="sf17eu-page-image index-text retro-text">
        <div class="retro-img-wrapper">
            <h1>SharkFest'17 EUROPE Retrospective</h1>
            <p>November 7-10, 2017<br>Palacio Estoril Hotel | Portugal</p>
        </div>
    </div>
    <section id="main-content">
        <div class="wrapper retrospective-page"">
           <!-- /HEADER -->
            <!-- Content area -->
            
            <div class="content-area">

                <!-- PAGE BLOG -->
                <section class="services">
                <div class="container" id="content2">
                <div class="row">

                <!-- Content -->
                <section id="content5" class="content col-sm-7 col-md-8 col-lg-8">

                    <article class="post-wrap" data-animation="fadeInUp" data-animation-delay="100">
                       <div class="post-media retro-image">
                        </div>
                        <div class="post-header">
                        </div>

                        <div class="post-body">
                            <div class="post-excerpt">     
                                <h3 class="post-title2">Keynote Presentation</h3>
                                
                                <div class="responsive-iframe">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/HiofIifbitQ" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <p>Wireshark and Expectations<br/>
                                Gerald Combs</p>
                                <h3 class="post-title2">SharkFest'17 Europe Movie</h3> 
                                <div class="responsive-iframe">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/WyIp_c5Rax4" frameborder="0" allowfullscreen></iframe>
                                </div>
                                
                                <h3 class="post-title2">Blogs</h3>
                                <p><a href="https://t.co/hnF26RYjT4?ssr=true">Sharkfest '17 Europe - First Thoughts </a> by Paul Offord <br>
                                    <a href="https://blog.packet-foo.com/2017/12/sharkfest-2017-eu-recap/">Sharkfest 2017 EU Recap</a> by Jasper Bongertz<br>
                                    <!--<a href="https://blog.packet-foo.com/2016/10/the-network-capture-playbook-part-2-speed-duplex-and-drops/">The Network Capture Playbook Part 2</a> by Jasper Bongertz<br>
                                    <a href="https://blog.packet-foo.com/2016/11/the-network-capture-playbook-part-3-network-cards/">The Network Capture Playbook Part 3</a> by Jasper Bongertz-->
                                </p>
                                
                                <h3 class="post-title2">Wednesday Classes</h3>
                                <ul style="list-style:none;">
                                    <li>01:<a href="assets/presentations17eu/01.pdf"> Hands-on TCP Analysis: Packets, Sequences & Fun</a> by Jasper Bongertz</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/R3nuuZiIbgU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:21:19)</li>
                                    </ul>
                                    
                                    <li>02:  <a href="assets/presentations17eu/02.pdf">Using Wireshark to Solve Real Problems for Real People: Step-by Step Case Studies in Packet Analysis</a> by Kary Rogers</li>
                                
                                    <li>03: <a href="assets/presentations17eu/03.7z">Writing a Wireshark Dissector: 3 Ways to Eat Bytes</a> by Graham Bloice</li>
                                    <!--<ul>
                                        <li><a href="assets/presentations16eu/01.zip"> Presentation Materials</a></li>
                                    </ul>-->
                                    <!--<ul>
                                        <li class="presVideo"><a href="https://youtu.be/Gs6Ogvh7uKY?t=51m54s" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:16:32)</li>
                                    </ul>-->
                                    <li>04: <a href="assets/presentations17eu/04.pptx">Augmenting Packet Capture with Contextual Meta-Data: the What, Why & How </a>by Dr. Stephen Donnelly</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/V88t6AnHyiY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (54:58)</li>
                                    </ul>
                  
                                <li>05:  <a href="assets/presentations17eu/05.pdf">Troubleshooting WLANs (Part 1): Layer 1 & 2 Analysis Using AirPcap, Wi-Spy & Other Tools</a> by Rolf Leutert</li>
                                <!--<ul>
                                <li class="presVideo"><a href="https://youtu.be/ec3UOed2knk" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:19:36)</li>
                                </ul>-->
                                <li>06: <a href="assets/presentations17eu/06.pdf"> Generating Wireshark Dissectors from XDR Files: Why you don’t want to write them by hand</a> by Richard Sharpe</li>
                                <li>07: <a href="https://start.me/p/Vw0GEN/sharkfest">SMB/CIFS Analysis: Using Wireshark to Efficiently Analyze & Troubleshoot SMB/CIFS</a> by Betty DuBois</li>
                                
                               <!--<ul>
                                <li class="presVideo"><a href="https://youtu.be/oDaDY9QCnXk"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:15:44)</li>
                                </ul> -->
                                <li>08:<a href="assets/presentations17eu/08.pdf">  Troubleshooting WLANs (Part 2): Troubleshooting WLANs Using 802.11 Management & Control Frames</a> by Rolf Leutert</li>
                                <!--<ul>
                                <li><a href="assets/presentations16eu/08.zip"> Presentation Materials</a></li>
                                </ul>-->
                                <li>09: Developer Bytes Lightning Talks–Development Track by Wireshark Core Developers</li> 
                                <!--<ul>
                                    <li class="presVideo"><a href="https://youtu.be/KbXc3c_Uboo"     title="Presentation video on YouTube" target="_blank">Presentation Video</a> (0:51:36)</li>
                                </ul>-->
                                <li>10:   SMB Handshake: The Devil Lies in the Detail by Eduard Blenkers</li>
                                
                                <ul>
                                    <li class="presVideo"><a href="https://youtu.be/L5U9dPR4UCY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (56:15)</li>
                                </ul>
                          
                            
                                <li>11:  Wireshark & Time: Accurate Handling of Time when Capturing Frames by Werner Fischer</li>
                               

                                <li>12: Developer Bytes Lightning Talks–Usage Track by Wireshark Core Developers</li>
                                <li>13:<a href="assets/presentations17eu/13.pdf">  Practical Tracewrangling: exploring trace file manipulation/extraction scenario </a> by Jasper Bongertz</li>
                                <ul>
                                <li class="presVideo"><a href="https://youtu.be/xzCNEB40DT0" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:06:08)</li>
                                </ul>
                                <li>14: <a href="assets/presentations17eu/14.pdf">Transmission Control Protocol Illustrated: everything you always wanted to know about TCP* (*but were afraid to ask)</a>  by  Ulrich Heilmeier</li>
                                <li>15: <a href="assets/presentations17eu/15.pdf">SSL/TLS Decryption: uncovering secrets</a> by Peter Wu</li> 
                                </ul>
                            <h3 class="post-title2">Thursday Classes</h3>
                            <ul style="list-style:none;">

                                <li>16: <a href="assets/presentations17eu/16.pdf">My TCP ain’t your TCP: Stack behavior back then, now and in the future </a> by Simon Lindermann</li> 
                                <ul>
                                <li class="presVideo"><a href="https://youtu.be/8RI9wtmcpeY" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:07:39)</li>
                                </ul>
                                <li>17:  <a href="assets/presentations17eu/17.pdf">Troubleshooting 802.11 with Monitoring Mode: Finding Patterns in your pcap</a> by Thomas Baudelet</li>
                                <!--<ul>
                                <li class="presVideo"><a href="https://youtu.be/rU-PR1p1Njc" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:12:01)</li>
                                </ul>-->
                               
                                <li>18:  extcap - Packet Capture beyond libpcap/winpcap: Bluetooth sniffing, Android dumping & other fun stuff by Roland Knall</li>
                                <li>19: <a href="assets/presentations17eu/19.pdf"> Turning Wireshark into a Traffic Monitoring Tool: Moving from packet details to the big picture</a> by Luca Deri</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/dp5FJMhxzio" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:13:11)</li>
                                    </ul>
                                
                                <li>20: QUIC Dissection: Using Wireshark to Understand QUIC Quickly  by  Megumi Takeshita</li>
                                <li><a href="assets/presentations17eu/20.pdf">PDF</a><a href="assets/presentations17eu/20.zip">Supplemental Files</a></li>
                                <li>21:<a href="assets/presentations17eu/21.pdf"> Custom LUA dissectors to the rescue in root cause analysis</a>  by  Sake Blok</li>
                                <li>22: <a href="https://start.me/p/Vw0GEN/sharkfest">Troubleshooting Layer 7 with Wireshark: because you don’t know what you don’t know</a>  by  Betty DuBois</li>
                                <li>23: <a href="assets/presentations17eu/23.pdf">The Network is Slow! Finding the Root Cause of Slow Application Performance</a>  by  Lorna Robertshaw</li>
                                <li>24: <a href="assets/presentations17eu/24.zip">Slow Start & TCP Reno Demystified: How Congestion Avoidance Modes are Working</a>  by  Christian Reusch</li>
                                <li>25: Hands-On Analysis of Multi-Point Captures by Christian Landström</li>
                                <li>26: <a href="assets/presentations17eu/26.pdf">How Did They Do That? Network Forensic Case Studies</a>  by  Phill Shade</li>
                                <li>27: Developer Bytes Lightning Talks–Development Track  by  Wireshark Core Developers</li>
                                <li>28: <a href="assets/presentations17eu/28.pdf">Designing a Requirements-based Packet Capture Strategy</a>  by  John Pittle</li> 
                                <ul>
                                <li class="presVideo"><a href="https://youtu.be/8edHFtbDOTU" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:18:32)</li>
                                </ul>
                                <li>29: The Packet Doctors are In!  by  Drs. Bongertz, Landström, Blok</li>
                                <li>30: Developer Bytes Lightning Talks–Usage Track  by  Wireshark Core Developers</li>
                            </ul>
                            <h3 class="post-title2">Friday Classes</h3>
                            <ul style="list-style:none;">
                                <li>31: New Ways to Find the Cause of Slow Response Times  by  Paul Offord</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/aehMMf0VpNg" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:10:46)</li>
                                    </ul>
                                <li>32: Real World Troubleshooting Tales  by  Graeme Bailey</li>
                                <li>33: <a href="assets/presentations17eu/33.7z">TShark Command Line using PowerShell</a>  by  Graham Bloice</li>
                                <li>34: <a href="assets/presentations17eu/34.pdf">TCP Analysis – When Things Get Complicated</a>  by  Jasper Bongertz</li>
                                    <ul>
                                        <li class="presVideo"><a href="https://youtu.be/Tz6IfyfodKo" title="Presentation video on YouTube" target="_blank">Presentation Video</a> (1:17:16)</li>
                                    </ul>
                                <li>35: <a href="assets/presentations17eu/35.pdf">Sneaking in by the Back Door – Hacking the Non-Standard Layers with Wireshark (BYOD) </a> by  Phill Shade</li>
                                <li>36: Introduction to ICS Protocols  by  Thomas Bringewald & Lars Seifert</li>
                            </ul>
                            </div>
                        </div>
                 
                    </article> 
                </section>



                <!-- Content -->
                <hr class="page-divider transparent visible-xs"/>
                
                <!-- Sidebar -->
                <aside id="sidebar" class="sidebar col-sm-4 col-md-3 col-xs-8">
                     <div class="post-media">
                        <a style="font-size: 25px;" data-fancybox="sf17eu" alt="Click Here to View Pictures from Sharkfest '17 Europe!" href='img/sharkfest17eugallery/2.jpg'><img src="img/sharkfest17eugallery/sf17eu.jpg" ></a>
                            <a href='img/sharkfest17eugallery/4.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/6.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/7.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/8.jpg' data-fancybox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/9.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/10.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/11.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/12.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/13.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/14.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/15.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/16.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/16.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/17.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/17.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/18.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/19.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/20.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/20.4.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/20.5.jpg' data-fancybox="sf17eu"></a>
                            
                            
                            <a href='img/sharkfest17eugallery/21.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/22.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/22.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/23.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/23.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/24.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/24.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/24.4.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/25.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/25.5.jpg' data-fancybox="sf17eu"></a>

                            <a href='img/sharkfest17eugallery/26.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/26.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/27.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/27.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/28.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/29.jpg' data-fancybox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/31.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/32.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/33.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/34.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/35.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/36.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/37.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/38.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/39.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/40.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/41.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/42.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/43.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/44.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/45.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/46.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/47.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/48.jpg' data-fancybox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/49.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/50.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/51.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/52.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/53.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/54.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/55.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/56.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/57.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/58.jpg' data-fancybox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/59.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/60.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/61.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/62.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/63.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/64.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.2.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.3.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.4.jpg' data-fancybox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/65.5.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.6.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.7.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.8.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/66.jpg' data-fancybox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/67.jpg' data-fancybox="sf17eu"></a>
                        <!--<a style="font-size: 25px;" data-lightbox="sf17eu" alt="Click Here to View Pictures from Sharkfest '17 Europe!" href='img/sharkfest17eugallery/2.jpg'><img src="img/sharkfest17eugallery/sf17eu.jpg" ></a></div>
                           
                            <a href='img/sharkfest17eugallery/4.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/6.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/7.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/8.jpg' data-lightbox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/9.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/10.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/11.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/12.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/13.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/14.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/15.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/16.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/16.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/17.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/17.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/18.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/19.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/20.jpg' data-lightbox="sf17eu"></a>
                            
                            
                            <a href='img/sharkfest17eugallery/21.5jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/22.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/22.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/23.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/23.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/24.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/24.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/24.4.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/25.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/25.5.jpg' data-lightbox="sf17eu"></a>

                            <a href='img/sharkfest17eugallery/26.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/26.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/27.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/27.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/28.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/29.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/30.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/31.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/32.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/33.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/34.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/35.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/36.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/37.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/38.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/39.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/40.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/41.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/42.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/43.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/44.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/45.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/46.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/47.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/48.jpg' data-lightbox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/49.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/50.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/51.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/52.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/53.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/54.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/55.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/56.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/57.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/58.jpg' data-lightbox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/59.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/60.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/61.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/62.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/63.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/64.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.2.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.3.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.4.jpg' data-lightbox="sf17eu"></a>
                            
                            <a href='img/sharkfest17eugallery/65.5.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.6.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.7.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/65.8.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/66.jpg' data-lightbox="sf17eu"></a>
                            <a href='img/sharkfest17eugallery/67.jpg' data-lightbox="sf17eu"></a>-->
                </aside>

                </div>

                </div>


                </section>
                 

                <!-- /PAGE BLOG -->

            </div>
        </div>
    </aside>
</div>
</div>
</section>
</div>
</div>
</section>
</div>
<?php include "footer.php"; ?>