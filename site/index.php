<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

    <div class="container grid-container" id="main-container">
    <div class="container grid-container" id="main-container">
        <div class="background-image">
            <h3>Stay tuned for news about SharkFest'23 EUROPE!</h3>
        </div>
        <div class="row grid-row">
        </div>
        <section id="main-content">
            <div class="container col-md-12">
                <div class="col-sm-8 col-xs-12 column">
                    <div class="col-sm-6 col-xs-7 background-blue front-column1">
                        <div class="grid-cell">
                            <h2>About SharkFest</h2>
                            <p>SharkFest is an annual educational conference focused on sharing knowledge, experience and best practices among the Wireshark developer and user communities. </p>
                            <div class="sf-logo-about">
                                
                            </div>
                        </div>
                        <div class="about-button">
                            <a class="" href="about"><h4>Learn More</h4></a>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-5 front-agenda-image front-column1 grid-cell">

                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 column">
                    <div class="col-xs-12 background-blue front-column1">
                        <div class="">
                            <div class="grid-cell">
                                <h2>Lodging Info</h2>
                                <p>Stay with us at the amazing Palacio Estoril Hotel</p>
                                <!-- <div class="retro-homepage">
                                    
                                </div> -->
                            </div>
                            <div class="retro-image">
                                
                            </div>
                        </div>
                        <div class="about-button">
                            <a class="" href="lodging"><h4>More info</h4></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container col-md-12" style="margin-bottom: 40px;">
                <div class="col-sm-4 col-xs-12 column">
                    <div class="col-xs-12 background-blue front-column1">
                        <div class="">
                            <div class="grid-cell">
                                <h2>Registration is now closed.</h2>
                                <!-- <p> • <a href="https://sharkfesteurope.wireshark.org/register#headingThree">Next Generation Protocols & Advanced Network Analysis Class</a> (Nov 4th-5th)<br>

                                • <a href="https://sharkfesteurope.wireshark.org/register#headingTwo">SharkFest Conference</a> (Nov 5th-8th)</p>
                                
                                <p>We hope to see you at the Palacio Estoril to celebrate the 4th annual SharkFest EU with us!</p> -->
                                <!-- <p>Sign up on or before September 12th to receive an early bird discount!</p> -->
                            </div>
                            <div class="retro-image">
                                
                            </div>
                        </div>
                        <div class="about-button">
                            <a class="" href="register"><h4>Register Now</h4></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-xs-12 column">
                    <div class="col-sm-6 col-xs-7 background-blue front-column1">
                        <div class="grid-cell">
                            <h2>Become a Sponsor</h2>
                            <p>There are many options for supporting the Wireshark community through a SharkFest sponsorship. To read about this, please click the button below.</p>
                            <p><a class="" href="assets/sponsor-doc22.pdf"><h4>Download Sponsorship Form</h4></a></p>
                        </div>
                        <div class="about-button">
                            <a class="" href="sponsors"><h4>Learn More</h4></a>
                        </div>
                    </div>
                    <div class="col-xs-5 col-sm-6 front-sponsor-image front-column1 grid-cell">

                    </div>
                </div>
            </div>
        </section> 
    </div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>
