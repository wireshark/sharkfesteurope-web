<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/jean-paul-archier.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Jean Paul Archier, Owner, JPACONSEIL</h2>
		<p>
		Jean-Paul has been working as a system and network engineer for more than 30 years. Since 2010, he has run his own company and is mainly focused on network training and consultancy. He is the author of several books for the French publisher ENI  : VPN, IPv6, Cisco ASA, Postfix and regularly gives training sessions on Wireshark and other network related topics. Recently, a European VOIP  solution provider asked him to build and deliver Wireshark training sessions for its resellers, focused on SIP troubleshooting. As a certified trainer, he also delivers VPN and network security training for WatchGuard resellers and clients.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>