<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/phill-shade.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Phill Shade,Senior Forensics Investigator, Merlion’s Keep Consulting</h2>
		<p>
		Phillip D. “Sherlock” Shade is a Senior Network / Forensics Engineer at Merlion's Keep Consulting and Training. An internationally recognized Network Security and Forensics expert, Phill draws from his over 30 years of hands-on, real world experience as a Network Engineer and Security Consultant in Network Analysis, troubleshooting, and Cyber Forensics / Security. His presentations at seminars and road shows use a highly energetic, knowledgeable and informative style. A member of the Global Cyber Response Team (GCRT), FBI InfraGard, Computer Security Institute, the HTCIA, the IEEE, and Volunteer at Cyber Warfare Forum Initiative, he is a frequent consultant for numerous international security, technology and government agencies and a featured speaker at local, regional, national and international Security events.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>