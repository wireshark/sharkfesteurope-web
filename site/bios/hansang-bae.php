<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/hansang.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Hansang Bae, Packet Guru</h2>
		<p>
		Hansang Bae led the Network/Application Performance Engineering Team with direct responsibility for Packet Capture Infrastructure at Citi until July, 2012 when he joined Riverbed Technology as Director of Cascade Product Architecture. He then took on the role of Chief Scientist and CTO for the company, and, in 2019, stepped into the role of Senior VP of Business Development to help guide Riverbed along a new strategic business path.  Now that this business path is secured, Hansang has chosen to pursue new opportunities.  With his broad knowledge of protocol analysis in complex enterprise infrastructures, Hansang brings a unique perspective to packet analysis.
 </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>