<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/oliver-ripka.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Oliver-Tobias Ripka, Consultant & Trainer, Ripka - Security Consulting</h2>
		<p>
		Oliver-Tobias Ripka is a network security expert with focus on offensive security and network analysis at Ripka - Security Consulting. He started working as a network engineer in 2002 while he was finishing school. During his international studies in Germany, France, and Portugal, he focused on machine learning and databases and worked on testing global climate models at the Max Plank Institute. After his studies, he worked on Linux kernel programming and embedded devices for telemetry applications at Intelligent Sensing Anywhere. In 2010, Oliver became a Consultant and Trainer for Fast Lane within the Synerity team, where he developed hacking trainings, and performed pentesting and incident response. In 2013, he joined Airbus Defence and Space CyberSecurity as a network defender and pentester. Since 2015 he has been working freelance as a cybersecurity consultant and trainer. <br><br>
		</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>