<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/thomas-bringewald.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Thomas Bringewald - IT Engineer</h2>
		<p>
		While completing his Bachelors' degree in Computer Science, Thomas Bringewald found a connection with IT security, which led to a Masters' degree in network and information security. Then, as a security engineer, he designed and implemented concepts for endpoint and network perimeter security with a focus on ICS networks, requiring him to take the special requirements of ICS and SCADA into account. Lars Seifert and Thomas Bringewald are colleagues and first-time SharkFest presenters. </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>