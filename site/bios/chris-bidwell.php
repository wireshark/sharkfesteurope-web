<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/andre-luyer.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Chris Bidwell</h2>
		<p>
		Chris has 15+ years working with enterprise financial networks and troubleshooting them with Wireshark. A self-described packet-head who loves to crack open Wireshark on the weekend whenever the opportunity arises, and encourages you all to try the Packet Capture Challenge!</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>