<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/milorad-imbra.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Milorad Imbra, CTO, LinkMyCoach</h2>
		<p>
		Software Architect specialized in distributed systems, integrations, cloud services and API design. Worked on many enterprise integration projects, as well on SaaS applications with startups. Uses Wireshark regularly for troubleshooting the integrations. Thinks that Wireshark goes well with RabbitMQ, Elasticsearch and Jenkins.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>