<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-12">
		<h2>Rami AlTalhi, Incident Response Consultant at Cisco Talos Incident Response (CTIR)</h2>
		<p>
		Rami has experience across different information security and cybersecurity fields for over 12 years. Worked as Incident Response Expert in the
        past for four years to handle different cyber incident and events. Provided DFIR and Cyber Range training for different regions in the world
        (Europe, Asia, Middle East and US). Dealt with different sophisticated APT cyber incident cases that ranging from cyber espionage until data
        destruction.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>