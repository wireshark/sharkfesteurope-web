<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/andre-luyer.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Andre Luyer, Performance Consultant, Rabobank</h2>
		<p>
		André is a senior Performance Consultant and troubleshooter at Rabobank and has been analyzing packets for over 25 years. First as a troubleshooter for network issues, both hard- and software, and later specializing in performance testing which requires a combination of in depth knowledge of networking protocols and coding skills, which is also used for testing security in the form of DDoS testing. André also is a trainer for an in-house ‘Wireshark bootcamp’ course.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>