<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/roland.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Roland Knall - Wireshark Core Developer</h2>
		<p>
		25 years of software development experience, still looking for those puzzles that evade an obvious solution. Main focus these days is UI design as well as accessibility of software. And to boldly go where no packets have gone before...Core Developer of Wireshark since 2016 and focuses mainly on the integration of external capture devices as well as UI improvements. </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>