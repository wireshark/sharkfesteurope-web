<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<!--<div class="col-sm-3">
		<img src="/img/speakers-large/kevin-burns.jpg">
	</div>-->
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>Bradley Duncan, Threat Intelligence Analyst, Palo Alto Networks - Unit 42</h2>
		<p>
		Brad Duncan specializes in network traffic analysis and exploit kit detection. After more than 21 years of classified intelligence work for the US Air Force, Brad transitioned to cyber security in 2010. He is currently a Threat Intelligence Analyst for Palo Alto Networks Unit 42. Brad is also a volunteer handler for the Internet Storm Center (ISC) and has posted more than 80 diaries at isc.sans.edu. He routinely blogs technical details and analysis of infection traffic at www.malware-traffic-analysis.net, where he's provided over 900 pcaps of malicious activity to the community.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>