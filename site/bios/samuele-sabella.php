<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/samuele-sabella.jpg">
	</div>
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>Samuele Sabella, Software Developer, ntop</h2>
		<p>
		Samuele is an undergraduate student at the Computer Science department of the University of Pisa, actually enrolled in the master's degree. His interests are network monitoring and machine learning, the main topic of his study plan.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>