<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/vladimir-gerasimov.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Vladimir Gerasimov, Network Engineer, Packettrain.NET</h2>
		<p>
		Vladimir Gerasimov currently works as a Network Engineer for Unitop LTD - a company building networks and IP video surveillance systems for customers. He has been working in IT for more than 12 years, and 7 years ago he shifted to Network Protocol Analysis with the main focus on TCP and Application performance analysis. Vladimir runs personal blog and he is also a creator and administrator of the largest Russian-speaking group regarding Network Protocol Analysis.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>