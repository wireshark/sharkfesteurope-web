<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/mike-canney.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Mike Canney, Packet Analyst</h2>
		<p>
		Mike Canney is a long-time packet analysis guru and a true entrepreneur, Mike has worked independently as well as for APM and NPM vendors. Professionally, Mike enjoys solving the most difficult problems given packet traces and personally, spending time with his young and growing family!</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>