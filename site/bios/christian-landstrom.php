<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/christian-landstrom.png">
	</div>
	<div class="col-sm-9">
		<h2>Christian Landström, Head of Managed Security Services, GDATA Advanced Analytics</h2>
		<p>
		Christian Landström works as Head of Managed Security Services at GDATA Advanced Analytics.
		Working in IT from 2004, with a strong focus on network communications and IT security, he
		graduated in computer science in 2008 and joined Synerity Systems and afterwards moved with the
		whole Synerity team to work for Fast Lane GmbH. There Christian created and delivered various
		Network Analysis Trainings and worked as Senior Consultant for network analysis and IT security. In
		2013 he started working for Airbus Defence and Space CyberSecurity, where he stayed until 2019 as
		Incident Response and security audit expert. He shares his passion about network analysis together
		with Jasper and Eddi from the original Synerity Team on the Sharkfest conferences and on
		blog.packet-foo.com.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>