<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/christian-reusch.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Christian Reusch - Network Engineer, CRnetPACKETS</h2>
		<p>
		Christian has been analyzing networks with Wireshark/Ethereal since 2000, has a great passion for packet analysis, and now maintains a private network blog – CrnetPackets.com. For his day job, he works as a network engineer for interlocking systems at Siemens AG.  Before his current job, he employed his considerable packet analysis skills for more than 5 years for 2nd and 3rd level network support in the financial sector.   </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>