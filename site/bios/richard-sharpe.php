<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/richard-sharpe.png">
	</div>
	<div class="col-sm-9">
		<h2>Richard Sharpe - Principal Software Engineer, Primary Data & Wireshark Core Developer</h2>
		<p>
		Richard Sharpe is an open source software developer who has written a number of Wireshark dissectors. He has also contributed to the Samba project. </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>