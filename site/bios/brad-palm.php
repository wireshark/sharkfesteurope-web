<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/brad-palm.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Brad Palm, Lead Network Analyst, USMC MCNEL</h2>
		<p>
		Brad is a network analyst for the United States Marine Corps, working in the Cyber Engineering and Network Efficiency Lab (MCNEL). His team focuses on network capture and analysis for USMC networks, testing emerging technologies and devices for the USMC Systems Command, and utilizing captured traffic to influence test scenarios that better represent the USMC network traffic profile in developmental testing phases. Brad has completed in-depth vendor studies/comparisons for WAN optimization devices, supported Tier1-level programs/applications in troubleshooting and optimization tasks for the USMC Enterprise Network, and deployed in support of tactical units to provide network analysis. His team uses a combination of TAPs, high speed capture devices, Riverbed Packet Analyzer, and Wireshark to accomplish their mission.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>