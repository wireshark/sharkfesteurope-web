<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/peter-wu.PNG">
	</div>
	<div class="col-sm-9">
		<h2>Peter Wu - Wireshark Core Developer</h2>
		<p>
		Peter Wu is part of the Crypto Team at Cloudflare, working on various TLS and cryptography-related projects. He is a contributor to many open source projects, including Wireshark, where he started in 2013 with TLS decryption improvements in order to assist in analyzing encrypted application traffic. Peter added TLS 1.3, QUIC, and WireGuard decryption support to Wireshark and aims to help everyone understand their traces.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>