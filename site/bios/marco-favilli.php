<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/marco-favilli.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Marco Favilli, Student, Pisa University</h2>
		<p>
		Born in Pisa (Italy), Marco is a computer science student at Pisa University.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>