<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/graeme-bailey.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Graeme Bailey, Troubleshooter & Founder, TARCA</h2>
		<p>
		Graeme is a UK-based troubleshooter with over 35 years experience in all aspects of system and infrastructure, having worked for Burroughs, HP, 3Com and others. He founded TARCA (troubleshooting and root cause analysis) in 2008, having identified a clear need for an independent consultancy firm with the capability to address end to end performance. Taking network analysis further than the network itself, TARCA encompasses applications, workstations, servers, storage, networks and connectivity, to provide a unique, unbiased insight into issues. TARCA helps to resolve problems for their clients more rapidly, often bringing together a wide variety of third parties and gaining agreement as to the precise cause of the issue. This maximises productivity potential for both people and equipment, often resulting in huge savings through the improvements they make together.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>