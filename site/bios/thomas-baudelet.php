<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<!-- <img src="/img/speakers-large/thomas-baudelet.jpeg"> -->
	</div>
	<div class="col-sm-9">
		<h2>Thomas Baudelet - Network & Security Analyst, iwaxx Sarl</h2>
		<p>
		After graduating as an engineer in 2003, Thomas worked for 3 years for Alcatel-Lucent as an integrator of GPRS services for national telecom operators in countries such as Russia, Nigeria, Egypt, United Arab Emirates, Thailand, Yemen, Togo, & Sri Lanka. In 2006, he joined the Geneva University Hospitals network team and worked on network and security technologies, including firewalls, VPNs, routing, administration of a public class B network, wireless, PKI, load balancers, and VoIP. He also set up the actual WLAN architecture: 1,800 Access Points with PKI authentication. In 2010, he created his own company, iwaxx Sarl, and since then has been offering his services in troubleshooting, network analysis, integration and training to private banks, public organizations, and international organizations. Thomas is the creator and main developer of Debookee, an OS X network analyzer that uses Wireshark at its core level for Ethernet and Wi-Fi monitoring captures.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>