<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/lars-seifert.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Lars Seifert - IT/Cyber Security, Airbus Group</h2>
		<p>
		Lars Siefert will be co-presenting Session 21 on November 9th, with his Airbus Group colleague Thomas Bringewald. </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>