<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/chris-greer.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Chris Greer - Network Analyst</h2>
		<p>
		Chris is a Network Analyst for Packet Pioneer, a professional services and training company. He regularly assists companies in tracking down the source of pesky network and application performance problems using a variety of protocol analysis and monitoring tools including Wireshark. Chris also delivers training and develops technical content for several analysis vendors. In addition to digging through trace files, Chris enjoys helping others to learn the art of analysis via onsite courses, blogging, and his YouTube channel. <br><br>
		<a href="http://www.packetpioneer.com">www.packetpioneer.com</a></p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>