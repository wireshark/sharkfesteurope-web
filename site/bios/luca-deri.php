<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/luca-deri.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Luca Deri - Network developer, ntop founder</h2>
		<p>
		Luca is the leader of the ntop project (<a href="http://www.ntop.org">http://www.ntop.org/</a>), which is aimed at developing an open-source monitoring platform for high-speed traffic analysis. He shares his time between ntop and the University of Pisa, where he is a lecturer in the Computer Science department.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>