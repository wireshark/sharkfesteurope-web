<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/john-pittle.jpg">
	</div>
	<div class="col-sm-9">
		<h2>John Pittle, Services CTO, Riverbed Technology, Inc.</h2>
		<p>
		Actively focused on Performance Engineering for networks, systems, and applications since the early 90s. Performance troubleshooting is my passion and joy. I've used NG Sniffer, HP Network Advisor, Ethereal, Wireshark, NetShark, AppResponse, Packet Analyzer, Transaction Analyzer, IT Guru, and the list goes on... Sr. Performance Consultant with OPNET Technologies since 2005, then came to Riverbed with the OPNET acquisition in 2012. Promoted to Distinguished Performance Consultant in 2015 reflecting expertise in the entire portfolio of Riverbed visibility and analysis products; as well as technical leadership within the consulting practice for our most complex customer engagements.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>