<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/matthias-kaiser.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Matthias Kaiser, Senior Trainer and Consultant, ExperTeach GmbH</h2>
		<p>
		Matthias started his career in network analysis in 1996 when he joined Network General as a Sniffer University European staff instructor. In various positions, he delivered Sniffer University training and coordinated the European instructor team that included experts like Rolf, Eddi and Jasper. In 2004, working as a freelance instructor and network consultant, he started working with Wireshark and wrote his own courseware on troubleshooting networks with Wireshark. Since 2009, Matthias has been working for ExperTeach, a German training and consulting company, where he manages and teaches the ExperTeach packet analysis curriculum for business customers. He has trained many individuals on Wireshark and helped them identify network and application-related problems.
</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>