<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/clement-notin.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Clément Notin, Security Researcher at Tenable Security</h2>
		<p>
		Clément Notin has been a cybersecurity engineer for around ten years. He started to learn about programming and networking, when Wireshark was still called Ethereal!
        He started as a pentester and auditor, first in a consulting company, then, for a global French industrial group.
        He is now a researcher in Active Directory security for Tenable in order to contribute to the Tenable.ad product that allows to identify in real time the weaknesses of such environments and detect the attacks underway.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>