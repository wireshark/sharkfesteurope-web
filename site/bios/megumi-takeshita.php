<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/megumi-takeshita.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Megumi Takeshita, Packet Otaku and Owner, Ikeriri Network Service</h2>
		<p>
		Megumi Takeshita, or Packet Otaku (Twitter: @ikeriri) runs a packet analysis company, Ikeriri Network Service, after having worked at BayNetworks and Nortel Networks in Japan. Ikeriri concerns packet analysis for troubleshooting, debugging and security inspection. Ikeriri is also a reseller of many wired/wireless capture and analysis devices and software for Riverbed, Metageek, Profitap, Dualcomm etc. Megumi has authored 10+ books about Wireshark and packet analysis in Japanese and she is one of contributors to the Wireshark project too.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>