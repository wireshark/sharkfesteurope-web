<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/lorna.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Lorna Robertshaw - Senior Solutions Engineer</h2>
		<p>
		Lorna Robertshaw spent the last 16 years working for OPNET and Riverbed as a sales engineer and subject matter expert on performance management solutions. She's always enjoyed the challenge of using packet traces to troubleshoot application and network performance problems and solve mysteries for her customers. Lorna is now taking on these same challenges as an independent contractor, using her extensive experience with Wireshark, along with Riverbed's Packet Analyzer and Transaction Analyzer, to provide packet analysis and other performance management services. Lorna earned a BSc in Computer Science at the University of Virginia and also enjoys rock climbing, video games, and hiking. </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>