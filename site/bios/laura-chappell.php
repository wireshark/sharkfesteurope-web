<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/laura-chappell.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Laura Chappell - Founder of Wireshark University</h2>
		<p>
		Laura Chappell is an American researcher and educator. She is best known as the founder of Wireshark University. She has authored two publications on Wireshark:</p>

		<ul>
			<li>Wireshark Network Analysis: the Official Wireshark Certified Network Analyst Study Guide</li>
			<li>Wireshark 101: Essential Skills for Network Analysis</li>
		</ul>
		<p>Laura Chappell's career began in 1989 at Novell, Inc., where she developed an interest in newly emergent Internet and networking technology. She subsequently worked as a road show presenter and course developer.</p>

		<p>In October 1983, she founded her first company, the Protocol Analysis Institute, where she served as a Senior Protocol and Security Analyst for 22 years, while also delivering presentations and trainings on network forensics for the High Technology Crime Investigation Association (HTCIA).</p>

		<p>In December 2006, Ms. Chappell founded Wireshark University, which provides a variety of training modalities, including onsite, self-paced, recorded and web-based formats. In January 2009, Ms. Chappell founded Chappell University, which has a broader focus, training networking professionals in the use of a variety of TCP/IP topics and tools.</p></p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>