<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/matteo-biscosi.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Matteo Biscosi, Software Engineer, Ntop</h2>
		<p>
		Italian born, Matteo graduated from the University of Pisa in Software Engineering. He is currently working in Ntop (networking company) as both Front-End and Back-End software engineer. He has already lectured at Fosdem in February 2021 and SharkFest in June 2021, mainly talking about cybersecurity attacks and network traffic analysis.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>