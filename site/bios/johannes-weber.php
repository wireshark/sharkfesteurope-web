<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/johannes-weber.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Johannes Weber, Senior Security Engineer, LBBW Asset Management</h2>
		<p>
		Johannes works as a senior security engineer at LBBW Asset Management, financial services. He has a master's degree in IT-Security (thesis:
		IPv6 Security) and blogs regularly at <a href="https://weberblog.net">https://weberblog.net</a>, covering IPv6, VPNs, DNSSEC, NTP, Wireshark, and other topics. Johannes works with
		next-generation firewalls, mail- and DNS-appliances, as well as classical routers/switches.<br><br>
		</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>