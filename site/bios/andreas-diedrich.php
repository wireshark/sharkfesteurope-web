<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/andreas-diedrich.jpeg">
	</div>
	<div class="col-sm-9">
		<h2>Andreas Diedrich, CEO, Interview Netzwerk Service GmbH</h2>
		<p>
		Andreas has worked 25 years in the IT industry, since 1995 has been a sniffer expert working in many scenerios with capture technology and is a reseller of Niksun, Opnet, Ixia and more. He focused for performance and fault analysis in large networks. For 4 years he has been working it software development, specifically with incidnet correlation and analytics. Also for the past 2 years he's worked on IPAC - TM development of Software for using Wireshark as monitoring platform.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>