<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<!--<div class="col-sm-3">
		<img src="/img/speakers-large/kevin-burns.jpg">
	</div>-->
	<div class="col-sm-9 col-centered" style="margin-top: 10px;">
		<h2>Mike McAlister, Lead Technologist, Booz Allen Hamilton</h2>
		<p>
		Mr. McAlister is a US Navy Veteran who earned his GCIH, and CSM with over 10 years of experience in Protocol Analyst. Recently Booz Allen Hamilton won the DFRWS SDN digital forensics challenge with the support of Joseph Bull, Chris Christou, Tyler Duquette, Emre Ertekin, Michael Lundberg, Michael McAlister and Greg Starkey. Booz Allen Hamilton advocates for open source solutions such as Wireshark to further advance SDN and the associated forensics tradecraft.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>