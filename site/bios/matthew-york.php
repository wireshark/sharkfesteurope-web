<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/matthew-york.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Matthew York, Performance & Stability Engineer Advance7</h2>
		<p>
		After having spent 10 years at a large enterprise software company as a software and solution architect, Matthew joined Advance7 in 2013 as a Performance & Stability Consultant. He now works with clients in multiple industries to troubleshoot intermittent application problems in distributed systems - things like slow response times, unknown errors, etc. He uses Wireshark on a daily basis to capture and analyse data and mainly works with large organisations, so tends to deal with capturing and post-processing large data sets. Matthew leads projects to build analytic tools for data captured with Wireshark, so has a really good understanding of the Pcap and PcapNg file formats. Matthew also holds Riverbed Application Performance Management and Network Performance Management certifications. <br><br>
		</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>