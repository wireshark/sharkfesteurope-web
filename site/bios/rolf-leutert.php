<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/rolf.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Rolf Leutert - Owner, Leutert NetServices</h2>
		<p>
		Rolf Leutert is a SNIFFER Certified Master (SCM) and Wireshark Certified Network Analyst (WCNA). Leutert NetServices (LNS) is a small team of highly-qualified network experts. For more than 20 years, we have offered trainings, troubleshooting, and consulting in protocol analysis all over Europe. LNS was the first company to offer Network General's Sniffer trainings and, in 2006 the first to offer Wireshark trainings in Europe. LNS has trained thousands of students all over Europe in renowned companies from A(TT) to Z(urich Insurance). The trainings are very practice-based, incorporating our many years of troubleshooting experience. </p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>