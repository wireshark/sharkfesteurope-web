<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/kary-rogers.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Kary Rogers - Chief Packet Officer @ <a href="www.packetbomb.com">Packetbomb.com</a></h2>
		<p>
		I first learned the value of packet analysis helping customers solve difficult issues in Riverbed TAC and have since moved onto a management role. Not wanting to lose the skills I fought hard to learn, I started a packet analysis website, <a href="www.packetbomb.com">PacketBomb.com</a>, where I post tutorials and case studies for the hapless network engineer struggling to prove that it's not the network.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>