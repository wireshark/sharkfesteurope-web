<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<!-- <img src="/img/speakers-large/maher.jpg"> -->
	</div>
	<div class="col-sm-9">
		<h2>Mark Stout, Principle Design Engineer, T-Mobile</h2>
		<p>
		Design, and Tech Support in Code Division Multiple Access (CDMA) and Long-Term Evolution (LTE) mobile networks, and now 5G for the last 21 years, in multiple countries. Active contributor to 3rd Generation Partnership Project (3GPP) 23, and 29 series. Currently the Lead Support Engineer for T-Mobile’s LTE, Voice Over LTE (VoLTE), Internet of Things (IoT),and true 5G technology on the Packet Core network.</p>
		
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>