<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/werner-fischer.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Werner Fischer, Principal Networking Consultant, avodaq AG</h2>
		<p>
		WWerner Fischer is a long-term Dual-CCIE (R/S, Security) with over 20 years of experience in the networking arena. At avodaq, Werner works as a Principal Networking Consultant on System Architectures. He provides design guidance in key projects and is responsible for transferring new technology of networking solutions to internal and external audiences. Werner holds numerous industry certificates and has been a Sniffer Certified Master since 2003, VMware Certified Professional (4/5/6) and has also attained the Gold Certified Engineer status from the IPv6 Forum. Prior to joining avodaq 10 years ago, Werner worked as a Network Project Engineer for Siemens AG.  SharkFester 2018 - serving around the globe - Singapore, Mountain View,CA and Vienna,AT - different continents but using and teaching the same tool everywhere - Wireshark. That's also Werner! For more details about his Biography, have a look at the different retrospective pages of former SharkFests.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>