<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/betty.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Betty DuBois - Chief Detective @ Network Detectives</h2>
		<p>
		Betty DuBois is the Chief Network Detective for Network Detectives, LLC. She has been analyzing networks since 1997, performing fault isolations, application profiles, and network baselines for a wide variety of clients. As an Instructor for Wireshark University, she is known for her ability to make a dry, complex subject fun and interesting, by using both humor and real-world examples.</p>
		<p>She has presented at both Sharkfest and Networld+Interop.</p>

		<p>Betty’s industry certifications include Certified Wireshark University Instructor, Wireshark Certified Network Analyst, HP ProCurve AIS, and Sniffer Certified Expert.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>