<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="container-fluid speakers-page">
	<div class="col-sm-3">
		<img src="/img/speakers-large/ulrich.jpg">
	</div>
	<div class="col-sm-9">
		<h2>Ulrich Heilmeier - DevSecOps Engineer - Vitesco Technology</h2>
		<p>
		Uli has been a network protocol enthusiast for years, and he believes in RFCs and sharing knowledge. He has been working as a DevSecOps engineer at Vitesco Technology.</p>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . "/footer.php"); ?>