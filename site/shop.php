<?php include($_SERVER['DOCUMENT_ROOT'] . "/header.php"); ?>

<div class="text-centered">
    <section id="main-content">
        <div class="container">
            <div id="myShop">
                <a href="https://sharkfest.myspreadshop.com">SharkFest Shop</a>
            </div>

            <script>
                var spread_shop_config = {
                    shopName: 'sharkfest',
                    locale: 'us_US',
                    prefix: 'https://sharkfest.myspreadshop.com',
                    baseId: 'myShop'
                };
            </script>

            <script type="text/javascript"
                    src="https://sharkfest.myspreadshop.com/shopfiles/shopclient/shopclient.nocache.js">
            </script>
        </div>
    </section>
</div>

<?php include($_SERVER[DOCUMENT_ROOT] . "/footer.php"); ?>
